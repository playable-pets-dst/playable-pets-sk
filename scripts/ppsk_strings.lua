
------------------------------------------------------------------
-- Recipe strings
------------------------------------------------------------------
--STRINGS.RECIPE_DESC.OINC100P = "Time to become the 1%"
------------------------------------------------------------------
-- Character select screen strings
------------------------------------------------------------------
STRINGS.CHARACTER_TITLES.grimalkin_cursep = "Margrel"
STRINGS.CHARACTER_NAMES.grimalkin_cursep = "Margrel"
STRINGS.CHARACTER_DESCRIPTIONS.grimalkin_cursep = "*Is a Giant\n*Has a passive curse aura\n*Can summon Black Kats\n*Summons bullets passively"
STRINGS.CHARACTER_QUOTES.grimalkin_cursep = "Despite being the unluckiest kat, you need an awful lot of luck to fight him."

STRINGS.CHARACTER_TITLES.grimalkinp = "Grimalkin"
STRINGS.CHARACTER_NAMES.grimalkinp = "Grimalkin"
STRINGS.CHARACTER_DESCRIPTIONS.grimalkinp = "*Is a Grimalkin\n*Ignores shields\n*Hates day time"
STRINGS.CHARACTER_QUOTES.grimalkinp = "Charlie but cooler."

STRINGS.CHARACTER_TITLES.mewkatp = "Mew Kat"
STRINGS.CHARACTER_NAMES.mewkatp = "Mew Kat"
STRINGS.CHARACTER_DESCRIPTIONS.mewkatp = "*Is always calm\n*Is adorable\n*Will never attack"
STRINGS.CHARACTER_QUOTES.mewkatp = "Mew Kat believes in you."

STRINGS.CHARACTER_TITLES.spookat_blackp = "Black Kat"
STRINGS.CHARACTER_NAMES.spookat_blackp = "Black Kat"
STRINGS.CHARACTER_DESCRIPTIONS.spookat_blackp = "*Is always angry\n*Loves eating books\n*Can summon zombies"
STRINGS.CHARACTER_QUOTES.spookat_blackp = "He would LOVE to eat your homework."

STRINGS.CHARACTER_TITLES.spookat_poisonp = "Hurkat"
STRINGS.CHARACTER_NAMES.spookat_poisonp = "Hurkat"
STRINGS.CHARACTER_DESCRIPTIONS.spookat_poisonp = "*Is a spookat\n*Has a dark side\n*Is a ghost"
STRINGS.CHARACTER_QUOTES.spookat_poisonp = "The only cure is death."

STRINGS.CHARACTER_TITLES.spookat_firep = "Pepperkat"
STRINGS.CHARACTER_NAMES.spookat_firep = "Pepperkat"
STRINGS.CHARACTER_DESCRIPTIONS.spookat_firep = "*Is a spookat\n*Has a dark side\n*Is a ghost"
STRINGS.CHARACTER_QUOTES.spookat_firep = "The only cure is death."

STRINGS.CHARACTER_TITLES.spookat_freezep = "Bloogato"
STRINGS.CHARACTER_NAMES.spookat_freezep = "Bloogato"
STRINGS.CHARACTER_DESCRIPTIONS.spookat_freezep = "*Is a spookat\n*Has a dark side\n*Is a ghost"
STRINGS.CHARACTER_QUOTES.spookat_freezep = "The only cure is death."

STRINGS.CHARACTER_TITLES.spookat_nrmp = "Spookat"
STRINGS.CHARACTER_NAMES.spookat_nrmp = "Spookat"
STRINGS.CHARACTER_DESCRIPTIONS.spookat_nrmp = "*Is a spookat\n*Has a dark side\n*Is a ghost"
STRINGS.CHARACTER_QUOTES.spookat_nrmp = "Cute until its not."

STRINGS.CHARACTER_TITLES.spookat_shockp = "Statikat"
STRINGS.CHARACTER_NAMES.spookat_shockp = "Statikat"
STRINGS.CHARACTER_DESCRIPTIONS.spookat_shockp = "*Is a spookat\n*Has a dark side\n*Is a ghost"
STRINGS.CHARACTER_QUOTES.spookat_shockp = "Shocking in more ways than one."

STRINGS.CHARACTER_TITLES.zombie_dustp = "Dust Zombie"
STRINGS.CHARACTER_NAMES.zombie_dustp = "DUST ZOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.zombie_dustp = "*Is a zombie\n*Raises fallen opponents as Zombies\n*wip"
STRINGS.CHARACTER_QUOTES.zombie_dustp = "The original bony boi"

STRINGS.CHARACTER_TITLES.zombie_firep = "Slag Walker"
STRINGS.CHARACTER_NAMES.zombie_firep = "SLAG WALKER"
STRINGS.CHARACTER_DESCRIPTIONS.zombie_firep = "*Is a zombie\n*Raises fallen opponents as Zombies\n*wip"
STRINGS.CHARACTER_QUOTES.zombie_firep = "The most familiar Zombie of all"

STRINGS.CHARACTER_TITLES.zombie_poisonp = "Droul"
STRINGS.CHARACTER_NAMES.zombie_poisonp = "DROUL"
STRINGS.CHARACTER_DESCRIPTIONS.zombie_poisonp = "*Is a zombie\n*Raises fallen opponents as Zombies\n*wip"
STRINGS.CHARACTER_QUOTES.zombie_poisonp = "Hes just DROULing to meet you"

STRINGS.CHARACTER_TITLES.zombie_freezep = "Frozen Shambler"
STRINGS.CHARACTER_NAMES.zombie_freezep = "FROZEN SHAMBLER"
STRINGS.CHARACTER_DESCRIPTIONS.zombie_freezep = "*Is a zombie\n*Raises fallen opponents as Zombies\n*wip"
STRINGS.CHARACTER_QUOTES.zombie_freezep = "what 99% of wilson's turn into"

STRINGS.CHARACTER_TITLES.zombie_shockp = "Frankenzom"
STRINGS.CHARACTER_NAMES.zombie_shockp = "FRANKEZOM"
STRINGS.CHARACTER_DESCRIPTIONS.zombie_shockp = "*Is a zombie\n*Raises fallen opponents as Zombies\n*wip"
STRINGS.CHARACTER_QUOTES.zombie_shockp = "A zombie mixed with Frankenstein monster? Shocking."

STRINGS.CHARACTER_TITLES.zombie_cursep = "Carnavon"
STRINGS.CHARACTER_NAMES.zombie_cursep = "CARNAVON"
STRINGS.CHARACTER_DESCRIPTIONS.zombie_cursep = "*Is a zombie\n*Raises fallen opponents as Zombies\n*Blocks attacks behind him"
STRINGS.CHARACTER_QUOTES.zombie_cursep = "He curses a lot. Fitting for how edgy he looks."

STRINGS.CHARACTER_TITLES.bombie_dustp = "Bombie"
STRINGS.CHARACTER_NAMES.bombie_dustp = "BOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.bombie_dustp = "*Is a bombie\n*Explodes when attacking\n*Can stun enemies"
STRINGS.CHARACTER_QUOTES.bombie_dustp = "The original bombie boi"

STRINGS.CHARACTER_TITLES.bombie_firep = "Burning Bombie"
STRINGS.CHARACTER_NAMES.bombie_firep = "BURNING BOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.bombie_firep = "*Is a bombie\n*Explodes when attacking\n*Can set enemies on fire"
STRINGS.CHARACTER_QUOTES.bombie_firep = "When you need more destruction in your explosions"

STRINGS.CHARACTER_TITLES.bombie_poisonp = "Choking Bombie"
STRINGS.CHARACTER_NAMES.bombie_poisonp = "CHOKING BOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.bombie_poisonp = "*Is a bombie\n*Explodes when attacking\n*Can poison enemies"
STRINGS.CHARACTER_QUOTES.bombie_poisonp = "How can he choke people when he has no arms?"

STRINGS.CHARACTER_TITLES.bombie_freezep = "Freezing Bombie"
STRINGS.CHARACTER_NAMES.bombie_freezep = "FREEZING BOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.bombie_freezep = "*Is a bombie\n*Explodes when attacking\n*Can freeze enemies"
STRINGS.CHARACTER_QUOTES.bombie_freezep = "Works well with other bombies for obvious reasons"

STRINGS.CHARACTER_TITLES.bombie_shockp = "Surging Bombie"
STRINGS.CHARACTER_NAMES.bombie_shockp = "SURGING BOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.bombie_shockp = "*Is a bombie\n*Explodes when attacking\n*Can shock enemies"
STRINGS.CHARACTER_QUOTES.bombie_shockp = "Hes just surging with excitement!"

STRINGS.CHARACTER_TITLES.bombie_cursep = "Cursing Bombie"
STRINGS.CHARACTER_NAMES.bombie_cursep = "CURSING BOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.bombie_cursep = "*Is a bombie\n*Explodes when attacking\n*Can curse enemies"
STRINGS.CHARACTER_QUOTES.bombie_cursep = "Hes going to drop the F bomb on you"

------------------------------------------------------------------
-- The prefab names as they appear in-game
------------------------------------------------------------------
--STRINGS.NAMES.CITY_LAMPP = "Street Light"

------------------------------------------------------------------
--Reforged Strings
------------------------------------------------------------------
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.zombie_dustp = "*Raises fallen opponents as Zombies\n*20% to inflict stun\n*Remembers skills of a prior life\n\nExpertise:\nBooks, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.zombie_cursep = "*Raises fallen opponents as Zombies\n*20% to inflict fire or curse\n*Has a 80% shield on his back\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.zombie_shockp = "*Raises fallen opponents as Zombies\n*20% to inflict shock\n*Shocks enemies when attacked\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.zombie_freezep = "*Raises fallen opponents as Zombies\n*20% to inflict freeze\n*Cannot be knocked back\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.zombie_poisonp = "*Raises fallen opponents as Zombies\n*20% to inflict poison\n*Immune to acid\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.zombie_firep = "*Raises fallen opponents as Zombies\n*20% to inflict fire\n*Attack damage increased by 50%\n\nExpertise:\nNone"

STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bombie_dustp = "*Explodes when attacking\n*50% to inflict stun\n*Can't revive others\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bombie_firep = "*Explodes when attacking\n*50% to inflict fire\n*Can't revive others\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bombie_freezep = "*Explodes when attacking\n*50% to inflict freeze\n*Can't revive others\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bombie_poisonp = "*Explodes when attacking\n*50% to inflict poison\n*Can't revive others\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bombie_shockp = "*Explodes when attacking\n*50% to inflict shock\n*Can't revive others\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bombie_cursep = "*Explodes when attacking\n*25% to inflict curse\n*Can't revive others\n\nExpertise:\nNone"

------------------------------------------------------------------
-- Mob strings
------------------------------------------------------------------
STRINGS.NAMES.SK_GRIM_TOTEM = "Grim Totem"

STRINGS.NAMES.SK_ZOMBIE = "Dust Zombie"
STRINGS.NAMES.SK_ZOMBIE_FIRE = "Slag Walker"
STRINGS.NAMES.SK_ZOMBIE_POISON = "Droul"
STRINGS.NAMES.SK_ZOMBIE_FREEZE = "Frozen Shambler"
STRINGS.NAMES.SK_ZOMBIE_SHOCK = "Frankenzom"
STRINGS.NAMES.SK_ZOMBIE_CURSE = "Carnavon"
STRINGS.NAMES.SK_ZOMBIE_BELLHOP = "Zombie Bellhop"

STRINGS.NAMES.SK_BOMBIE = "Bombie"
STRINGS.NAMES.SK_BOMBIE_FIRE = "Burning Bombie"
STRINGS.NAMES.SK_BOMBIE_POISON = "Choking Bombie"
STRINGS.NAMES.SK_BOMBIE_FREEZE = "Freezing Bombie"
STRINGS.NAMES.SK_BOMBIE_SHOCK = "Surging Bombie"

STRINGS.NAMES.SK_SPOOKAT = "Spookat"
STRINGS.NAMES.SK_SPOOKAT_FIRE = "Pepperkat"
STRINGS.NAMES.SK_SPOOKAT_POISON = "Hurkat"
STRINGS.NAMES.SK_SPOOKAT_FREEZE = "Bloogato"
STRINGS.NAMES.SK_SPOOKAT_SHOCK = "Statikat"
STRINGS.NAMES.SK_SPOOKAT_BLACK = "Black Kat"
STRINGS.NAMES.SK_SPOOKAT_BLACK_T2 = STRINGS.NAMES.SK_SPOOKAT_BLACK
STRINGS.NAMES.SK_SPOOKAT_MEW = "Mew Kat"
STRINGS.NAMES.SK_SPOOKAT_RITUAL = "Ritual Kat"
--STRINGS.NAMES.SK_BOMBIE_CURSE = "Carnavon"
------------------------------------------------------------------
-- Pigman-specific speech strings
------------------------------------------------------------------

--STRINGS.CHARACTERS.PIGMANPLAYER = require "speech_pigmanplayer"

------------------------------------------------------------------
-- The default responses of characters examining the prefabs
------------------------------------------------------------------

--[[
STRINGS.CHARACTERS.GENERIC.DESCRIBE.CITY_LAMPP = 
{
	GENERIC = "It turns on at night.",
}
]]

------------------------------------------------------------------
-- NPC strings
------------------------------------------------------------------

return STRINGS