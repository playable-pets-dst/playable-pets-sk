require "behaviours/chaseandattack"
require "behaviours/wander"
require "behaviours/faceentity"
require "behaviours/follow"
require "behaviours/standstill"
require "behaviours/runaway"
require "behaviours/doaction"
require "behaviours/panic"
require "behaviours/leash"


local MAX_WANDER_DIST = 7
local MIN_FOLLOW_LEADER = 3
local MAX_FOLLOW_LEADER = 10
local TARGET_FOLLOW_LEADER = 5

local function GetLeader(inst)
    return inst.components.follower ~= nil and inst.components.follower.leader or nil
end

local function GetHomePos(inst)
    local home = GetLeader(inst)
    return home ~= nil and home:GetPosition() or nil
end

local function GetNoLeaderLeashPos(inst)
    return GetLeader(inst) == nil and GetHomePos(inst) or nil
end

local function GetWanderPoint(inst)
    local target = GetLeader(inst)
    return target ~= nil and target:GetPosition() or nil
end

local function GetLeaderLeashPos(inst)
    local leader = GetLeader(inst)
    if leader == nil or inst.leader_offset == nil then
        return
    end
    local x, y, z = leader.Transform:GetWorldPosition()
    return Vector3(x + inst.leader_offset.x, 0, z + inst.leader_offset.z)
end


local SK_ZombieBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

function SK_ZombieBrain:OnStart()
    local root = PriorityNode(
    {
        WhileNode(function() return self.inst.components.combat.target end, "Attacking", ChaseAndAttack(self.inst, TUNING.SK_ZOMBIES.TARGET_TIME)),
		Follow(self.inst, GetLeader, MIN_FOLLOW_LEADER, TARGET_FOLLOW_LEADER, MAX_FOLLOW_LEADER),
		Wander(self.inst, GetWanderPoint(self.inst), MAX_WANDER_DIST),
    }, .25)

    self.bt = BT(self.inst, root)
end

return SK_ZombieBrain
