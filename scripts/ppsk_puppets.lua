local DEFAULT_SCALE = 0.20
local Puppets = {
	--SK
	zombie_dustp = {bank = "sk_zombie", build = "sk_zombie_dust", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "sk_zombie_dust"},
	zombie_firep = {bank = "sk_zombie", build = "sk_zombie_fire", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "sk_zombie_fire"},
	zombie_freezep = {bank = "sk_zombie", build = "sk_zombie_freeze", anim = "idle_loop", scale = DEFAULT_SCALE - 0.04, shinybuild = "sk_zombie_freeze"},
	zombie_poisonp = {bank = "sk_zombie", build = "sk_zombie_poison", anim = "idle_loop", scale = DEFAULT_SCALE - 0.06, shinybuild = "sk_zombie_poison"},
	zombie_shockp = {bank = "sk_zombie", build = "sk_zombie_shock", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "sk_zombie_shock"},
	zombie_cursep = {bank = "sk_zombie", build = "sk_zombie_curse", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "sk_zombie_curse"},
	bombie_dustp = {bank = "sk_zombie", build = "sk_bombie_dust", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "sk_bombie_dust"},
	bombie_firep = {bank = "sk_zombie", build = "sk_bombie_fire", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "sk_bombie_fire"},
	bombie_freezep = {bank = "sk_zombie", build = "sk_bombie_freeze", anim = "idle_loop", scale = DEFAULT_SCALE - 0.04, shinybuild = "sk_bombie_freeze"},
	bombie_poisonp = {bank = "sk_zombie", build = "sk_bombie_poison", anim = "idle_loop", scale = DEFAULT_SCALE - 0.06, shinybuild = "sk_bombie_poison"},
	bombie_shockp = {bank = "sk_zombie", build = "sk_bombie_shock", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "sk_bombie_shock"},
	--Kats
	spookat_nrmp = {bank = "sk_spookat", build = "sk_spookat_nrm", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "sk_spookat_nrm"},
	mewkatp = {bank = "sk_spookat", build = "sk_spookat_mewkat", anim = "idle_loop", scale = DEFAULT_SCALE/2, shinybuild = "mewkat"},
	spookat_shockp = {bank = "sk_spookat", build = "sk_spookat_shock", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "sk_spookat_nrm"},
	spookat_poisonp = {bank = "sk_spookat", build = "sk_spookat_poison", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "sk_spookat_nrm"},
	spookat_firep = {bank = "sk_spookat", build = "sk_spookat_fire", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "sk_spookat_nrm"},
	spookat_freezep = {bank = "sk_spookat", build = "sk_spookat_freeze", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "sk_spookat_nrm"},
	spookat_blackp = {bank = "sk_spookat", build = "sk_spookat_black", anim = "idle_loop_angry", scale = DEFAULT_SCALE + 0.05, shinybuild = "sk_spookat_black"},
	grimalkinp = {bank = "sk_grimalkin", build = "sk_grimalkin_nrm", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "sk_spookat_black"},
	grimalkin_cursep = {bank = "sk_grimalkin", build = "sk_grimalkin_curse", anim = "idle_loop", scale = DEFAULT_SCALE + 0.05, shinybuild = "sk_spookat_black"},
}

return Puppets