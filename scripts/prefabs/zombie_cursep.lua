local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "zombie_cursep"

local assets = 
{
	Asset("ANIM", "anim/sk_zombie.zip"),
	Asset("ANIM", "anim/sk_zombie_dust.zip"),
	Asset("ANIM", "anim/sk_zombie_freeze.zip"),
	Asset("ANIM", "anim/sk_zombie_shock.zip"),
	Asset("ANIM", "anim/sk_zombie_poison.zip"),
	Asset("ANIM", "anim/sk_zombie_fire.zip"),
	Asset("ANIM", "anim/sk_zombie_curse.zip"),
}

local prefabs = 
{	
	"weaponsparks_fx"
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 750,
	hunger = 100,
	hungerrate = 0, 
	sanity = 50,
	
	runspeed = 8,
	walkspeed = 3,
	
	attackperiod = 2,
	damage = 60,
	range = 7,
	hit_range = 3,
	
	bank = "sk_zombie",
	build = "sk_zombie_curse",
	shiny = "sk_zombie_curse",
	
	scale = 0.9,
	stategraph = "SGzombiep",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('zombie_cursep',
{
    {"boneshard",       1.00},
    {"boneshard",   	1.00},
	{"nightmarefuel",  	0.50},
})

local sounds =
{
	grunt = "sk/creatures/zombie/grunt",
    attack = "sk/creatures/zombie/attack",
	attack2 = "sk/creatures/zombie/attack2",
    hit = "sk/creatures/zombie/hit",
    death = "sk/common/hit/hit_zombie",
    spawn = "sk/creatures/zombie/spawn",
	attack3_pre = "sk/creatures/zombie/attack3_pre",
	attack3 = "sk/creatures/zombie/attack3",
}

--==============================================
--					Mob Functions
--==============================================
local function OnHitOther(inst, other, damage, stimuli)
    if other and (not stimuli or stimuli ~= "status") then
		PlayablePets.InflictStatus(inst, other, "fire", 0.2)
	end
end

--==============================================
--				Custom Common Functions
--==============================================
local function IsBlocked(inst, afflicter)
	if afflicter then
		local anglediff = inst.Transform:GetRotation() - inst:GetAngleToPoint(afflicter.Transform:GetWorldPosition())
		if not (math.abs(anglediff) <= 90) then
			return true
		else
			return false
		end
	else
		return false
	end
end

local function redirecthealth(inst, attacker, damage, weapon, stimuli)--(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
    if amount < 0 and IsBlocked(inst, afflicter) then
		inst.isblocked = true
        inst.SoundEmitter:PlaySound("sk/common/hit/hit_invincible")
	elseif amount < 0 and not IsBlocked(inst, afflicter) then
		inst.isblocked = nil
    end   
	return inst
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.variant = data.variant or nil
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.variant = inst.variant or nil
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst, forgestats)
	PlayablePets.SetForgeStats(inst, forgestats or PPSK_FORGE.ZOMBIE)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees","darts","books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("zombie")
	inst:AddTag("monster")	
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 1) --fire, acid, poison
	--inst.components.combat.redirectdamagefn = redirecthealth
	local _oldGetAttacked = inst.components.combat.GetAttacked
	inst.components.combat.GetAttacked = function(self, attacker, damage, weapon, stimuli)
		local absorb = 0
		if IsBlocked(inst, attacker) then
			SpawnPrefab("sk_hit_sparksp").Transform:SetPosition(inst:GetPosition():Get())
			inst.SoundEmitter:PlaySound("sk/common/hit/hit_invincible")
			absorb = 0.8
			inst.isblocked = true
		end
		if self.inst.components.health.absorb ~= absorb then self.inst.components.health:SetAbsorptionAmount(absorb) end
		return _oldGetAttacked(self, attacker, damage, weapon, stimuli)
	end

	
	PlayablePets.SetFamily(inst, SK_FAMILIES.UNDEAD) --inst, prefaboverride
	PlayablePets.SetCommonStatusResistances(inst, nil, 0, nil, nil, nil, 0) --(inst, stun, fire, shock, poison, freeze, curse, sleep)
	----------------------------------
	--Variables	
	inst.variant = "_curse"
	
	inst.hit_recovery = 0.75
	
	inst.mobsleep = true
	
	inst.taunt = true
	
	inst.shouldwalk = true
	
	inst.altattack = true
	
	inst.sounds = sounds
	
	inst.getskins = getskins
	
	local body_symbol = "rib"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	----------------------------------
	--Light--
	inst.glow = true
	inst.Light:Enable(true)
	inst.Light:SetRadius(3)
	inst.Light:SetFalloff(1.6)
	inst.Light:SetIntensity(0.25)
	inst.Light:SetColour(244/255, 69/255, 255/255)
	
	
	local fx = SpawnPrefab("sk_enemy_shieldp") --todo, not showing up for clients
	fx.Transform:SetScale(1.25, 1.25, 1.25)
	fx.AnimState:PlayAnimation("back")
    fx.entity:SetParent(inst.entity)
	inst.fx = fx
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(2,2,2) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	inst:SetPhysicsRadiusOverride(0.5)
	MakeCharacterPhysics(inst, 10, 0.5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2.5, 1.5)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("killed", PlayablePets.ZombieOnKill)
	inst.components.combat.onhitotherfn = OnHitOther
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)