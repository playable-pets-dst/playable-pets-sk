local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "grimalkin_cursep"

local assets = 
{
	Asset("ANIM", "anim/sk_grimalkin.zip"),
	Asset("ANIM", "anim/sk_grimalkin_curse.zip"),
}

local prefabs = 
{	
	"weaponsparks_fx"
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.GRIMALKIN_CURSEP_HEALTH,
	hunger = TUNING.GRIMALKIN_CURSEP_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.GRIMALKIN_CURSEP_SANITY,
	
	runspeed = 7/3,
	walkspeed = 7/3,
	
	attackperiod = 2,
	damage = 100*2,
	range = 3,
	hit_range = 4,
	
	bank = "sk_grimalkin",
	build = "sk_grimalkin_curse",
	shiny = "sk_grimalkin",
	
	scale = 3,
	stategraph = "SGgrimalkinp",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('grimalkinp',
{

})

local sounds =
{
	grunt = "sk/creatures/margrel/growl",
	moan = "sk/creatures/margrel/moan",
    attack = "sk/creatures/grimalkin/bite",
	spawn = "sk/creatures/grimalkin/spawn",
}
--==============================================
--					Mob Functions
--==============================================
local function OnHitOther(inst, other, damage, stimuli)
    if other and (not stimuli or stimuli ~= "status") then
		if SK_CURSE_ENABLE then
			PlayablePets.InflictStatus(inst, other, "curse", 0.1)
		end
	end
end

--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.variant = data.variant or nil
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.variant = inst.variant or nil
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst, forgestats)
	PlayablePets.SetForgeStats(inst, forgestats or PPSK_FORGE.BLACKKAT)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees","darts","books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("spookat")
	inst:AddTag("blackkat")
	inst:AddTag("undead")
	inst:AddTag("monster")	
	inst:AddTag("epic")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "shadow","kat", "undead"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	end
end

local function GetBulletExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "spookat", "undead", "smallcreature", "bird", "insect", "wall"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "spookat", "undead", "smallcreature", "bird", "insect", "wall"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "shadow", "spookat", "undead", "smallcreature", "bird", "insect", "wall"}
	end
end

local function SuggestTarget(inst, target)
	for i, v in pairs(inst.minions) do
		if v:IsValid() then
			v.components.combat.target = target
		end
	end
end

local function OnAttacked(inst, data)
	if data.attacker and data.attacker:IsValid() then
		SuggestTarget(inst, data.attacker)
	end
end

local function FireProjectile(inst, target, pos)
    if target then
		local target_pos = target:GetPosition()
		local bullet = SpawnPrefab("sk_bullet_chaser")
		bullet.Transform:SetPosition(pos.x, 0, pos.z)			
		bullet.components.projectile:Throw(inst, target)
		bullet.NOTAGS = GetExcludeTags(inst)
		bullet.owner = inst
		bullet.damage = TUNING.SK.MARGREL.BULLET_DAMAGE
		bullet.duration = TUNING.SK.MARGREL.BULLET_DURATION
    end
end

local function KillAllMinions(inst)
	if inst.minions then
		for i, v in pairs(inst.minions) do
			if v and v:IsValid() then
				v.components.health:Kill()
			end
			inst.minions[v] = nil
		end
	end
end

local function SpawnBullet(inst, target)
	if target then
		local pos = inst:GetPosition()
		local range = TUNING.SK.MARGREL.BULLET_SPAWN_RANGE
		local newpos = {x = pos.x + math.random(-range, range), y = pos.y, z = pos.z + math.random(-range, range)}
		local fx = SpawnPrefab("sk_marg_bullet_spawner")
		fx.Transform:SetPosition(newpos.x, 1.5, newpos.z)
		fx:ListenForEvent("animqueueover", function(spawner)
			local spawnpos = spawner:GetPosition()
			local hit_fx = SpawnPrefab("sk_bullet_explodep")
			hit_fx.AnimState:SetMultColour(180/255, 0/255, 52/255, 1)
			hit_fx.Transform:SetPosition(fx:GetPosition():Get())
			hit_fx.AnimState:SetScale(2.2, 2.2)
			if target and target:IsValid() and target.components.health and not target.components.health:IsDead() then
				FireProjectile(inst, target, {x = newpos.x, y = 1.5, z = newpos.z})
			end
			fx:Remove()
		end)
	end
end

local function DoBulletSpawn(inst)
	if inst.can_spawn_bullets and not inst.noactions and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
		--spawn homing bullets here
		local ent = FindEntity(inst, 30, function(guy)
                    return inst.components.combat:CanTarget(guy) and not (guy.components.follower and guy.components.follower.leader == inst)
                end,
                {"_combat"},
                GetBulletExcludeTags(inst))
		SpawnBullet(inst, ent)
	end
end

local function DoCursePulse(inst)
	if not inst.noactions and not inst.nocurse and not (not SK_CURSE_ENABLE and inst:HasTag("player")) and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
		local pos = inst:GetPosition()
		local aura = SpawnPrefab("sk_curse_aura")
		aura.Transform:SetPosition(pos:Get())
		aura.owner = inst
		
		local fx = SpawnPrefab("sk_marg_lightning_ground_fx")
		local newpos = {x = pos.x + math.random(-20, 20), y = 0, z = pos.z + math.random(-20, 20)}
		fx.Transform:SetPosition(newpos.x, 0, newpos.z)
		
		if math.random() < 0.1 then
			local fx2 = SpawnPrefab("sk_marg_lightning")
			local thunderpos = {x = pos.x + math.random(-20, 20), y = 0, z = pos.z + math.random(-20, 20)}
			fx2.Transform:SetPosition(thunderpos.x, 0, thunderpos.z)
		end
	end
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetFamily(inst, SK_FAMILIES.UNDEAD) 
	PlayablePets.SetCommonStatusResistances(inst, nil, nil, nil, nil, nil, 0) --(inst, stun, fire, shock, poison, freeze, curse, sleep)
	PlayablePets.SetPassiveHeal(inst)
	----------------------------------
	--Variables	
	inst.variant = "_curse"
	
	inst.hit_recovery = 1
	
	inst.specialsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.taunt3 = true
	inst._ismonster = true
	
	inst.can_spawn_bullets = true
	
	inst.minions = {}
	inst.SuggestTarget = SuggestTarget
	
	inst.shouldwalk = true
	inst.isangry = true
	
	inst.sounds = sounds
	
	local body_symbol = "swap_debuff"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	PlayablePets.SetCommonStatResistances(inst, 1, 0, 0, 9999) --fire, acid, poison
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Light--
	inst.AnimState:SetSymbolLightOverride("mouth", 1)
	inst.AnimState:SetSymbolLightOverride("eye", 1)
	inst.AnimState:SetSymbolLightOverride("swap_seal", 1)
	inst.glow = false
	inst.Light:Enable(false)
	inst.Light:SetRadius(3)
	inst.Light:SetFalloff(1.6)
	inst.Light:SetIntensity(0.25)
	inst.Light:SetColour(244/255, 69/255, 255/255)
	
	--[[
	local fx = SpawnPrefab("sk_enemy_shieldp") --todo, not showing up for clients
	fx.Transform:SetScale(1.25, 1.25, 1.25)
	fx.AnimState:PlayAnimation("back")
    fx.entity:SetParent(inst.entity)
	inst.fx = fx]]
	
	--inst.SoundEmitter:PlaySound("sk/creatures/spookat/ambience", "ghost_loop")
	inst:DoPeriodicTask(0.3, DoCursePulse)
	inst:DoPeriodicTask(TUNING.SK.MARGREL.BULLET_RATE, DoBulletSpawn)
	inst.fx = inst:DoPeriodicTask(0.3, function(inst)
		inst:DoTaskInTime(math.random()+1, function(inst)
			if not inst.noactions and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
				local fx = SpawnPrefab("sk_static_sparkp")
				local pos = inst:GetPosition()
				local offset = 2
				fx.Transform:SetPosition(pos.x + math.random(-offset, offset), (pos.y + (math.random(-offset, offset)/2)) + 3, pos.z + math.random(-offset, offset))
				fx.AnimState:SetMultColour((69/255)*0, (41/255)*0, (91/255)*0, 1)
				fx.AnimState:SetScale(2.2, 2.2)
				fx.AnimState:PlayAnimation(math.random(1, 4), false)
			end
		end)
	end)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.BOOK, FOODTYPE.MEAT, FOODTYPE.GENERIC, FOODTYPE.VEGGIE }, { FOODTYPE.BOOK, FOODTYPE.MEAT, FOODTYPE.GENERIC, FOODTYPE.VEGGIE })
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	inst:SetPhysicsRadiusOverride(0.5)
	MakeFlyingCharacterPhysics(inst, 10, 0.5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2.5, 1.5)
	inst.DynamicShadow:Enable(false)  

	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("onremove", KillAllMinions)
	inst:ListenForEvent("death", KillAllMinions)
	inst:ListenForEvent("pp_despawn", KillAllMinions)
	inst:ListenForEvent("attacked", OnAttacked)
	--inst.components.combat.onhitotherfn = OnHitOther
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)