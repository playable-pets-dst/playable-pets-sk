--require "scripts/sk_tuning"
--TODO clean this shit up!
local assets =
{
    Asset("ANIM", "anim/sk_zombie.zip"),
	Asset("ANIM", "anim/sk_zombie_dust.zip"),
	Asset("ANIM", "anim/sk_zombie_freeze.zip"),
	Asset("ANIM", "anim/sk_zombie_shock.zip"),
	Asset("ANIM", "anim/sk_zombie_poison.zip"),
	Asset("ANIM", "anim/sk_zombie_fire.zip"),
	Asset("ANIM", "anim/sk_zombie_curse.zip"),
	-------
	Asset("ANIM", "anim/sk_bombie_dust.zip"),
	Asset("ANIM", "anim/sk_bombie_freeze.zip"),
	Asset("ANIM", "anim/sk_bombie_shock.zip"),
	Asset("ANIM", "anim/sk_bombie_poison.zip"),
	Asset("ANIM", "anim/sk_bombie_fire.zip"),
}

local prefabs =
{
    
}

local brain = require("brains/sk_zombiebrain")

local sounds =
{
	grunt = "sk/creatures/bombie/grunt",
    attack = "sk/creatures/bombie/attack",
	attack2 = "sk/creatures/bombie/attack",
    hit = "sk/creatures/bombie/hit",
    death = "sk/common/hit/hit_zombie",
    spawn = "sk/creatures/zombie/spawn",
	explode = "sk/creatures/bombie/explode",
	fuse = "sk/creatures/bombie/fuse_loop"
}

SetSharedLootTable('sk_zombie',
{
    {"boneshard",       1.00},
    {"boneshard",   	1.00},
})

local t_values = TUNING.SK_ZOMBIES

local function IsBlocked(inst, afflicter)
	if afflicter then
		local anglediff = inst.Transform:GetRotation() - inst:GetAngleToPoint(afflicter.Transform:GetWorldPosition())
		return not (math.abs(anglediff) <= 90)
	else
		return false
	end
end

local function redirecthealth(inst, attacker, damage, weapon, stimuli)--(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
    if amount < 0 and IsBlocked(inst, afflicter) then
		inst.isblocked = true
        inst.SoundEmitter:PlaySound("sk/common/hit/hit_invincible")
	elseif amount < 0 and not IsBlocked(inst, afflicter) then
		inst.isblocked = nil
    end   
	return inst
end

local function OnAttacked(inst, data)
	if not inst.components.follower.leader then
		inst.components.combat:SetTarget(data.attacker)
		inst.components.combat:ShareTarget(data.attacker, 30,
        function(dude)
            return not (dude.components.health ~= nil and dude.components.health:IsDead())
                and (dude:HasTag("zombie") or dude:HasTag("undead"))
                and data.attacker ~= (dude.components.follower ~= nil and dude.components.follower.leader or nil)
        end, t_values.TARGET_RANGE + 10)
	else
		if data.attacker and data.attacker ~= inst.components.follower.leader then
			inst.components.combat:SetTarget(data.attacker)
		end
	end
end

local function OnAttackOther(inst, data)
    inst.components.combat:ShareTarget(data.target, 20,
        function(dude)
            return not (dude.components.health ~= nil and dude.components.health:IsDead())
                and (dude:HasTag("zombie") or dude:HasTag("undead"))
                and data.target ~= (dude.components.follower ~= nil and dude.components.follower.leader or nil)
        end, t_values.TARGET_RANGE + 10)
end

local function OnHitOther(inst, other, damage, stimuli)
    if other and (not stimuli or stimuli ~= "status") then
		PlayablePets.InflictStatus(inst, other, "stun", 0.5)
	end
end

local function OnKillOther(inst, data)
	if data.victim and data.victim:IsValid() and (data.victim:HasTag("character") or data.victim:HasTag("player")) and not (data.victim:HasTag("largecreature") or data.victim:HasTag("giant")) then
		local pos = data.victim:GetPosition()
		local zombie = SpawnPrefab(inst.prefab)
		zombie.Transform:SetPosition(pos.x, 0, pos.z)
		zombie.sg:GoToState("spawn")
	
	elseif data.victim and data.victim:IsValid() and (data.victim:HasTag("character") or data.victim:HasTag("player")) and (data.victim:HasTag("largecreature") or data.victim:HasTag("giant")) then
	
	end
end

local function OnNewTarget(inst, data)
    if inst.components.sleeper:IsAsleep() then
        inst.components.sleeper:WakeUp()
    end
end

local function GetExcludeTags(inst)
	if TheNet:GetServerGameMode() == "lavaarena" or inst:HasTag("companion") or (inst.components.follower.leader and inst.components.follower.leader:HasTag("player") and not TheNet:GetPVPEnabled()) then
		return	{ "wall", "player", "companion", "_isinheals", "smallcreature", "insect"}
	else
		return { "wall", "zombie", "zombiefriend", "structure", "sk_enemy", "smallcreature", "insect", "undead" }
	end
end


local function retargetfn(inst)
    local leader = inst.components.follower.leader
    if leader and leader.components.combat and not leader.components.combat.target then
		return 
	elseif leader and leader.components.combat and leader.components.combat.target then
		return leader.components.combat.target
	end
	return FindEntity(
                inst,
                t_values.TARGET_RANGE,
                function(guy)
                    return inst.components.combat:CanTarget(guy) and guy ~= leader
                end,
                nil,
                GetExcludeTags(inst)
            )
end

local function KeepTarget(inst, target)
    local leader = inst.components.follower.leader
    return (leader == nil or inst:IsNear(leader, t_values.FOLLOWER_RETURN_DISTANCE))
        and inst.components.combat:CanTarget(target)
        and not (TheNet:GetServerGameMode() == "lavaarena" and target:HasTag("_isinheals"))
end

local function OnSave(inst, data)

end

local function OnLoad(inst, data)

end

local function OnAttackOther(inst, other, damage, stimuli)
    if other and (not stimuli or stimuli ~= "status") then
		if inst.variant and inst.variant == "freeze" and math.random(1, 100) <= 50 then
			if other.components.freezable then
				other.components.freezable:Freeze(8)
			end
		else
			PlayablePets.InflictStatus(inst, other, inst.variant or "stun", 0.5)
		end	
	end
end

local function fncommon()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddLight()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

	inst:SetPhysicsRadiusOverride(0.5)
    MakeCharacterPhysics(inst, 10, .5)

    inst.DynamicShadow:SetSize(2.5, 1.5)
    inst.Transform:SetFourFaced()
	local scale = 0.9
	inst.Transform:SetScale(scale, scale, scale)

    inst:AddTag("scarytoprey")
	inst:AddTag("zombie")
	inst:AddTag("bombie")
	inst:AddTag("monster")

    inst.AnimState:SetBank("sk_zombie")
    inst.AnimState:SetBuild("sk_bombie_dust")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.altattack = true
	
	inst.shouldwalk = false
	
	inst.hit_recovery = t_values.HIT_RECOVERY

    inst.sounds = sounds

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	inst.components.locomotor.walkspeed = t_values.WALK_SPEED
    inst.components.locomotor.runspeed = t_values.RUN_SPEED
    inst:SetStateGraph("SGsk_zombie")

    inst:SetBrain(brain)

    inst:AddComponent("follower")	
	
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(t_values.HEALTH/2)
	inst.components.health.nofadeout = true

    inst:AddComponent("sanityaura")
    inst.components.sanityaura.aura = -TUNING.SANITYAURA_MED

	inst:AddComponent("colouradder")
	
    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(t_values.EXPLODE_DAMAGE)
    inst.components.combat:SetAttackPeriod(t_values.ATTACK_PERIOD)
	inst.components.combat:SetRange(t_values.EXPLODE_ATTACK_RANGE, t_values.EXPLODE_HIT_RANGE)
    inst.components.combat:SetRetargetFunction(3, retargetfn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
    inst.components.combat:SetHurtSound("sk/common/hit/hit_zombie")
	inst.components.combat.onhitotherfn = OnAttackOther
	
    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('sk_zombie')

    inst:AddComponent("inspectable")
	
	inst:AddComponent("named")
	
	local fx = SpawnPrefab("torchfire_rag")
	fx.entity:SetParent(inst.entity)
	fx.entity:AddFollower()
	fx.Follower:FollowSymbol(inst.GUID, "swap_fuse", 0, 0, 0)
	inst.fuse = fx

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

	--inst:ListenForEvent("killed", PlayablePets.ZombieOnKill)
    inst:ListenForEvent("attacked", OnAttacked)
    inst:ListenForEvent("onattackother", OnAttackOther)
	
	PlayablePets.SetCommonStatResistances(inst, 2, 2) --fire, acid, poison
	
	PlayablePets.SetFamily(inst, SK_FAMILIES.UNDEAD) --inst, prefaboverride

    return inst
end

local function fndefault()
    local inst = fncommon()

    if not TheWorld.ismastersim then
        return inst
    end

	
    MakeMediumFreezableCharacter(inst, "rib")
    MakeMediumBurnableCharacter(inst, "rib")

    return inst
end


local function fnfire()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_bombie_fire")
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.variant = "fire"

    MakeMediumFreezableCharacter(inst, "rib")
	inst:RemoveComponent("burnable")

  	inst.Light:Enable(true)
	inst.Light:SetRadius(t_values.LIGHT_RADIUS)
	inst.Light:SetFalloff(t_values.LIGHT_FALLOFF)
	inst.Light:SetIntensity(t_values.LIGHT_INTENSITY)
	inst.Light:SetColour(180/255, 90/255, 30/255)

    return inst
end

local function fncurse()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_bombie_curse")
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.variant = "curse"

    MakeMediumFreezableCharacter(inst, "rib")
	inst:RemoveComponent("burnable")
    --inst.components.freezable:SetResistance(4) --because fire

  	inst.Light:Enable(true)
	inst.Light:SetRadius(t_values.LIGHT_RADIUS)
	inst.Light:SetFalloff(t_values.LIGHT_FALLOFF)
	inst.Light:SetIntensity(t_values.LIGHT_INTENSITY)
	inst.Light:SetColour(244/255, 69/255, 255/255)
	
	local fx = SpawnPrefab("sk_enemy_shieldp") --todo, not showing up for clients
	fx.Transform:SetScale(1.25, 1.25, 1.25)
	fx.AnimState:PlayAnimation("back")
    fx.entity:SetParent(inst.entity)
	inst.fx = fx
	
	local _oldGetAttacked = inst.components.combat.GetAttacked
	inst.components.combat.GetAttacked = function(self, attacker, damage, weapon, stimuli)
		local absorb = 0
		if IsBlocked(inst, attacker) then
			SpawnPrefab("sk_hit_sparksp").Transform:SetPosition(inst:GetPosition():Get())
			inst.SoundEmitter:PlaySound("sk/common/hit/hit_invincible")
			absorb = 1
			inst.isblocked = true
		end
		if self.inst.components.health.absorb ~= absorb then self.inst.components.health:SetAbsorptionAmount(absorb) end
		return _oldGetAttacked(self, attacker, damage, weapon, stimuli)
	end

    return inst
end

local function fnshock()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_bombie_shock")
	
    if not TheWorld.ismastersim then
        return inst
    end

	inst.variant = "shock"
	
    MakeMediumFreezableCharacter(inst, "rib")
	MakeMediumBurnableCharacter(inst, "rib")
	
	inst.Light:Enable(true)
	inst.Light:SetRadius(t_values.LIGHT_RADIUS)
	inst.Light:SetFalloff(t_values.LIGHT_FALLOFF)
	inst.Light:SetIntensity(t_values.LIGHT_INTENSITY)
	inst.Light:SetColour(69/255, 255/255, 202/255)

    return inst
end

local function fnice()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_bombie_freeze")
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.variant = "freeze"
	MakeMediumFreezableCharacter(inst, "rib")
	inst.components.freezable:SetResistance(9999)
	
	MakeMediumBurnableCharacter(inst, "rib")

    return inst
end

local function fnpoison()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_bombie_poison")
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.variant = "poison"

    MakeMediumFreezableCharacter(inst, "rib")
	MakeMediumBurnableCharacter(inst, "rib")
	
	inst.poisonimmune = true

    return inst
end

return Prefab("sk_bombie", fndefault, assets, prefabs),
		Prefab("sk_bombie_fire", fnfire, assets, prefabs),
		Prefab("sk_bombie_shock", fnshock, assets, prefabs),
		Prefab("sk_bombie_freeze", fnice, assets, prefabs),
		Prefab("sk_bombie_poison", fnpoison, assets, prefabs)
