--require "scripts/sk_tuning"
--TODO clean this shit up!
local assets =
{
    Asset("ANIM", "anim/sk_spookat.zip"),
	Asset("ANIM", "anim/sk_spookat_nrm.zip"),
	Asset("ANIM", "anim/sk_spookat_black.zip"),
	Asset("ANIM", "anim/sk_spookat_fire.zip"),
	Asset("ANIM", "anim/sk_spookat_freeze.zip"),
	Asset("ANIM", "anim/sk_spookat_shock.zip"),
	Asset("ANIM", "anim/sk_spookat_poison.zip"),
	Asset("ANIM", "anim/sk_spookat_mewkat.zip"),
	Asset("ANIM", "anim/sk_spookat_moorcraft.zip"),
}

local prefabs =
{
    
}

local brain = require("brains/sk_zombiebrain")

SetSharedLootTable('spookat',
{
    
})

local sounds =
{
	grunt = "sk/creatures/spookat/meow",
	taunt = "sk/creatures/spookat/mew",
    attack = "sk/creatures/spookat/attack",
	spit = "sk/creatures/spookat/spit",
    hit = "sk/creatures/blackkat/hit",
    death = "sk/creatures/spookat/death",
}

local sounds_black =
{
	grunt = "sk/creatures/blackkat/attack",
	taunt = "sk/creatures/blackkat/meow",
    attack = "sk/creatures/spookat/attack",
	spit = "sk/creatures/blackkat/spit",
    hit = "sk/creatures/blackkat/hit",
    death = "sk/creatures/blackkat/death",
}

local t_values = TUNING.SK_SPOOKATS

local function GetExcludeTags(inst)
	if TheNet:GetServerGameMode() == "lavaarena" or inst:HasTag("companion") or (inst.components.follower.leader and inst.components.follower.leader:HasTag("player") and not TheNet:GetPVPEnabled()) then
		return	{ "wall", "player", "companion", "_isinheals", "smallcreature", "insect"}
	else
		return { "wall", "zombie", "structure", "sk_enemy", "smallcreature", "insect", "undead" }
	end
end

local function FireProjectile(inst, target)
    if target then
		local pos = inst:GetPosition()
		local target_pos = target:GetPosition()
		local total_projectiles = t_values.NUM_PROJ
		local angledif = t_values.ANGLE_DIF
		for i = 0, total_projectiles do
			local angle = inst.Transform:GetRotation() + ((angledif * i) - angledif)
			local bullet = SpawnPrefab(inst.status and "sk_bullet_"..inst.status or "sk_bullet_shadow")
			bullet.Transform:SetPosition(pos:Get())			
			bullet.components.projectile:Throw(inst, target)
			bullet.Transform:SetRotation(angle)
			bullet.components.projectile:SetSpeed(t_values.BULLET_SPEED)
			bullet.NOTAGS = GetExcludeTags(inst)
			bullet.owner = inst
			bullet.damage = inst:HasTag("blackkat") and t_values.BULLET_DAMAGE*3 or t_values.BULLET_DAMAGE
		end	
    end
end


local function retargetfn(inst)
    local leader = inst.components.follower.leader
    if leader and leader.components.combat and not (leader.components.combat.target) then
		return 
	elseif leader and leader.components.combat and leader.components.combat.target then
		return leader.components.combat.target
	end
	return FindEntity(
                inst,
                inst:HasTag("mewkat") and 6 or t_values.TARGET_RANGE,
                function(guy)
                    return inst.components.combat:CanTarget(guy) and guy ~= leader and (not guy.components.follower or (guy.components.follower and guy.components.follower.leader ~= inst))
                end,
                nil,
                GetExcludeTags(inst)
            )
end

local function KeepTarget(inst, target)
    local leader = inst.components.follower.leader
    return (leader == nil or (leader and inst:IsNear(leader, t_values.FOLLOWER_RETURN_DISTANCE)))
        and inst.components.combat:CanTarget(target)
		and target ~= leader 
		and not (inst:HasTag("mewkat") and not inst:IsNear(target, 30))
        and not (TheNet:GetServerGameMode() == "lavaarena" and target:HasTag("_isinheals"))
end

local function OnSave(inst, data)

end

local function OnLoad(inst, data)

end

local function SetAngry(inst, mood)
	if not inst.ignoremood then
		inst.isangry = mood
		if mood then
			inst.shouldwalk = false
		else
			inst.shouldwalk = true
		end
		if not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("taunt") and not inst.sg:HasStateTag("attack") and inst.isangry ~= mood then
			inst.sg:GoToState(mood and "idle_to_angry" or "angry_to_idle")
		end
	end
end

local function OnHitOther(inst, other, damage, stimuli)
    if other and (not stimuli or stimuli ~= "status") then
		if inst.status and inst.status == "freeze" and math.random() <= t_values.INFLICT_CHANCE then
			if other.components.freezable then
				other.components.freezable:Freeze(8)
			end
		elseif inst.status then
			if inst.status == "curse" then
				if not (inst.components.follower.leader and inst.components.follower.leader:HasTag("player") and not SK_CURSE_ENABLE) and math.random(1, 10) >= 5 then
					PlayablePets.InflictStatus(inst, other, inst.status, t_values.INFLICT_CHANCE)
				else
					PlayablePets.InflictStatus(inst, other, "fire", t_values.INFLICT_CHANCE)
				end
			else
				PlayablePets.InflictStatus(inst, other, inst.status, t_values.INFLICT_CHANCE)
			end
		end	
	end
end

local function OnNewTarget(inst, data)
	SetAngry(inst, true)
end

local function OnDroppedTarget(inst, data)
	SetAngry(inst, false)
end

local function DoCursePulse(inst)
	if not inst.noactions and not inst.nocurse and not (not SK_CURSE_ENABLE and inst:HasTag("player")) and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
		local pos = inst:GetPosition()
		local aura = SpawnPrefab("sk_curse_aura")
		aura.Transform:SetPosition(pos:Get())
	end
end

local function QueueSummon(inst)
	if inst.components.combat.target and inst.can_summon and inst.components.leader:CountFollowers() < t_values.MAX_MINIONS then
		inst.summon_queued = true
	end
end

local function fncommon()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddLight()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

	inst:SetPhysicsRadiusOverride(0.5)
    MakeFlyingCharacterPhysics(inst, 10, .5)

    inst.DynamicShadow:SetSize(2.5, 1.5)
    inst.Transform:SetFourFaced()
	local scale = t_values.SCALE
	inst.Transform:SetScale(scale, scale, scale)

    inst:AddTag("scarytoprey")
	inst:AddTag("spookat")
	inst:AddTag("monster")
	inst:AddTag("undead")

    inst.AnimState:SetBank("sk_spookat")
    inst.AnimState:SetBuild("sk_spookat_nrm")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.isangry = false
	inst.shouldwalk = true
	inst._ismonster = true
	
	inst.hit_recovery = t_values.HIT_RECOVERY
	
	inst.FireProjectile = FireProjectile
	inst.SetAngry = SetAngry

    inst.sounds = sounds

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	inst.components.locomotor.walkspeed = t_values.WALK_SPEED
    inst.components.locomotor.runspeed = t_values.RUN_SPEED
    inst:SetStateGraph("SGsk_spookat")

    inst:SetBrain(brain)


	inst:AddComponent("leader")
	
    inst:AddComponent("follower")	
	inst.components.follower.keepleaderonattacked = true
	
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(t_values.HEALTH)

    inst:AddComponent("sanityaura")
    inst.components.sanityaura.aura = -TUNING.SANITYAURA_MED

	inst:AddComponent("colouradder")
	inst:AddComponent("colourtweener")
	
    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(t_values.DAMAGE)
    inst.components.combat:SetAttackPeriod(t_values.ATTACK_PERIOD)
	inst.components.combat:SetRange(t_values.ATTACK_RANGE, t_values.HIT_RANGE)
    inst.components.combat:SetRetargetFunction(1, retargetfn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
    inst.components.combat:SetHurtSound(nil)
	inst.components.combat.onhitotherfn = OnHitOther
	
	MakeMediumFreezableCharacter(inst, "kat_head")
	MakeMediumBurnableCharacter(inst, "kat_head")
	
    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('sk_spookat')

    inst:AddComponent("inspectable")
	
	inst.SoundEmitter:PlaySound("sk/creatures/spookat/ambience", "ghost_loop")

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

	inst:ListenForEvent("newcombattarget", OnNewTarget)
	inst:ListenForEvent("droppedtarget", OnDroppedTarget)
	
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	
	PlayablePets.SetFamily(inst, SK_FAMILIES.UNDEAD) --inst, prefaboverride

    return inst
end

local function fndefault()
    local inst = fncommon()

    if not TheWorld.ismastersim then
        return inst
    end

	inst.variant = ""

    return inst
end

local function fnfire()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_spookat_fire")
	
    if not TheWorld.ismastersim then
        return inst
    end

	inst.variant = "_fire"
	inst.status = "fire"

  	inst.Light:Enable(true)
	inst.Light:SetRadius(t_values.LIGHT_RADIUS)
	inst.Light:SetFalloff(t_values.LIGHT_FALLOFF)
	inst.Light:SetIntensity(t_values.LIGHT_INTENSITY)
	inst.Light:SetColour(180/255, 90/255, 30/255)

    return inst
end

local function fncurse()
    local inst = fncommon()
	
	inst.AnimState:SetLightOverride(0.85)
	inst.AnimState:PlayAnimation("idle_loop_angry")
	
	inst:AddTag("largecreature")
	inst:AddTag("blackkat")
	
	inst.DynamicShadow:SetSize(2.5, 1.5)

	inst.AnimState:SetBuild("sk_spookat_black")
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.components.health:SetMaxHealth(TUNING.SK_BLACKKAT.HEALTH)
	inst.components.combat:SetDefaultDamage(TUNING.SK_BLACKKAT.DAMAGE)
	inst.components.combat:SetRange(t_values.ATTACK_RANGE*1.2, t_values.HIT_RANGE)
	
	inst.components.locomotor.runspeed = t_values.WALK_SPEED/2
	
	inst.Transform:SetScale(2, 2, 2)

	inst.ignoremood = true
	inst.isangry = true
	inst.minions = {}
	inst.can_summon = true
	inst.variant = "_curse"
	inst.sounds = sounds_black
	inst:DoPeriodicTask(0.3, DoCursePulse)
	
	inst.fx = inst:DoPeriodicTask(0.3, function(inst)
		inst:DoTaskInTime(math.random()+1, function(inst)
			local fx = SpawnPrefab("sk_static_sparkp")
			local pos = inst:GetPosition()
			local offset = 1
			fx.Transform:SetPosition(pos.x + math.random(-offset, offset), (pos.y + (math.random(-offset, offset)/2)) + 3, pos.z + math.random(-offset, offset))
			fx.AnimState:SetMultColour((69/255)*0, (41/255)*0, (91/255)*0, 1)
			fx.AnimState:SetScale(2, 2)
			fx.AnimState:PlayAnimation(math.random(1, 4), false)
		end)
	end)
	
    return inst
end

local function fncurse_T2()
    local inst = fncommon()
	
	inst.AnimState:SetSymbolLightOverride("mouth", 1)
	inst.AnimState:SetSymbolLightOverride("eye", 1)
	inst.AnimState:PlayAnimation("idle_loop_angry")
	
	inst:AddTag("largecreature")
	inst:AddTag("blackkat")
	
	inst.DynamicShadow:SetSize(2.5, 1.5)

	inst.AnimState:SetBuild("sk_spookat_black")
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.components.health:SetMaxHealth(TUNING.SK_BLACKKAT.HEALTH*0.75)
	inst.components.combat:SetDefaultDamage(TUNING.SK_BLACKKAT.DAMAGE*0.75)
	inst.components.combat:SetRange(t_values.ATTACK_RANGE*1.1, t_values.HIT_RANGE)
	
	inst.components.locomotor.runspeed = t_values.WALK_SPEED/2
	
	inst.Transform:SetScale(1.5, 1.5, 1.5)

	inst.ignoremood = true
	inst.isangry = true
	inst.minions = {}
	inst.can_summon = true
	inst.variant = "_curse"
	inst.minionoverride = "sk_zombie"
	inst.sounds = sounds_black
	
	inst.fx = inst:DoPeriodicTask(0.3, function(inst)
		inst:DoTaskInTime(math.random()+1, function(inst)
			local fx = SpawnPrefab("sk_static_sparkp")
			local pos = inst:GetPosition()
			local offset = 1
			fx.Transform:SetPosition(pos.x + math.random(-offset, offset), (pos.y + (math.random(-offset, offset)/2)) + 3, pos.z + math.random(-offset, offset))
			fx.AnimState:SetMultColour((69/255)*0, (41/255)*0, (91/255)*0, 1)
			fx.AnimState:SetScale(1.5, 1.5)
			fx.AnimState:PlayAnimation(math.random(1, 4), false)
		end)
	end)
	
    return inst
end

local function fnshock()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_spookat_shock")
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.variant = "_shock"
	inst.status = "shock"

	inst.Light:Enable(true)
	inst.Light:SetRadius(t_values.LIGHT_RADIUS)
	inst.Light:SetFalloff(t_values.LIGHT_FALLOFF)
	inst.Light:SetIntensity(t_values.LIGHT_INTENSITY)
	inst.Light:SetColour(69/255, 255/255, 202/255)

    return inst
end

local function fnice()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_spookat_freeze")
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.variant = "_freeze"
	inst.status = "freeze"

    return inst
end

local function fnpoison()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_spookat_poison")
	
    if not TheWorld.ismastersim then
        return inst
    end

	inst.variant = "_poison"
	inst.status = "poison"

    return inst
end

local function fnmew()
    local inst = fncommon()
	
	inst:AddTag("mewkat")
	inst:RemoveTag("monster")
	inst.AnimState:SetBuild("sk_spookat_mewkat")
	
	MakeFlyingCharacterPhysics(inst, 5, .5)

    inst.DynamicShadow:SetSize(1.25, 0.75)
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.Transform:SetScale(0.5, 0.5, 0.5)

	inst.components.combat:SetRange(t_values.HIT_RANGE)
	
	inst.ignoremood = true
	inst.isangry = false
	inst.variant = ""
	
	inst.components.sanityaura.aura = TUNING.SANITYAURA_TINY

    return inst
end

return Prefab("sk_spookat", fndefault, assets, prefabs),
		Prefab("sk_spookat_fire", fnfire, assets, prefabs),
		Prefab("sk_spookat_black", fncurse, assets, prefabs),
		Prefab("sk_spookat_black_t2", fncurse_T2, assets, prefabs),
		Prefab("sk_spookat_shock", fnshock, assets, prefabs),
		Prefab("sk_spookat_freeze", fnice, assets, prefabs),
		Prefab("sk_spookat_poison", fnpoison, assets, prefabs),
		Prefab("sk_spookat_mew", fnmew, assets, prefabs)
