local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "spookat_blackp"

local assets = 
{
	Asset("ANIM", "anim/sk_spookat.zip"),
	Asset("ANIM", "anim/sk_spookat_black.zip"),
	
}

local prefabs = 
{	
	"weaponsparks_fx"
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 1250,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	
	runspeed = TUNING.SK_SPOOKATS.RUN_SPEED/2,
	walkspeed = 4/2,
	
	attackperiod = 2,
	damage = 75*2,
	range = 10,
	hit_range = 3,
	
	bank = "sk_spookat",
	build = "sk_spookat_black",
	shiny = "sk_spookat_black",
	
	scale = 2,
	stategraph = "SGspookatp",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('spookat_blackp',
{
    {"boneshard",       1.00},
    {"boneshard",   	1.00},
	{"nightmarefuel",  	0.50},
})

local sounds =
{
	grunt = "sk/creatures/blackkat/attack",
	taunt = "sk/creatures/blackkat/meow",
    attack = "sk/creatures/spookat/attack",
	spit = "sk/creatures/blackkat/spit",
    hit = "sk/creatures/blackkat/hit",
    death = "sk/creatures/blackkat/death",
}

--==============================================
--					Mob Functions
--==============================================
local function OnHitOther(inst, other, damage, stimuli)
    if other and (not stimuli or stimuli ~= "status") then
		if SK_CURSE_ENABLE then
			PlayablePets.InflictStatus(inst, other, "curse", 0.1)
		end
	end
end

local function SetMinionTarget(inst, target)
	if target and target:IsValid() and target.components.combat then
		for i, v in ipairs(inst.minions) do
			if v and v:IsValid() and v.components.combat then
				v.components.combat:SetTarget(target)
			end
		end
	end
end

local function DoCursePulse(inst)
	if not inst.noactions and not inst.nocurse and not (not SK_CURSE_ENABLE and inst:HasTag("player")) and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
		local pos = inst:GetPosition()
		local aura = SpawnPrefab("sk_curse_aura")
		aura.Transform:SetPosition(pos:Get())
	end
end

--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.variant = data.variant or nil
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.variant = inst.variant or nil
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst, forgestats)
	PlayablePets.SetForgeStats(inst, forgestats or PPSK_FORGE.BLACKKAT)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees","darts","books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("spookat")
	inst:AddTag("blackkat")
	inst:AddTag("undead")
	inst:AddTag("monster")	
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "shadow","kat", "undead"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	end
end

local function FireProjectile(inst, target)
    if target then
		local pos = inst:GetPosition()
		local target_pos = target:GetPosition()
		local total_projectiles = 2
		local angle_between_projectiles = 10
		local angledif = 30
		for i = 0, total_projectiles do
			local angle = inst.Transform:GetRotation() + ((angledif * i) - angledif)
			local bullet = SpawnPrefab("sk_bullet_shadow")
			bullet.Transform:SetPosition(pos:Get())			
			bullet.components.projectile:Throw(inst, target)
			bullet.Transform:SetRotation(angle)
			bullet.NOTAGS = GetExcludeTags(inst)
			bullet.owner = inst
			bullet.damage = mob.damage/2
		end	
		
		--[[
		local bullet = SpawnPrefab("sk_bullet_shadow")
		bullet.Transform:SetPosition(pos:Get())
		bullet:DoTaskInTime(0, function(inst) bullet.components.projectile.owner = inst end)
		bullet.components.projectile.owner = inst
		bullet.components.projectile:Throw(inst, target)]]
    end
end

local function KillAllMinions(inst)
	if inst.minions then
		for i, v in pairs(inst.minions) do
			if v and v:IsValid() then
				v.components.health:Kill()
			end
			inst.minions[v] = nil
		end
	end
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	
	PlayablePets.SetFamily(inst, SK_FAMILIES.UNDEAD) 
	PlayablePets.SetCommonStatusResistances(inst, nil, nil, nil, nil, nil, 0) --(inst, stun, fire, shock, poison, freeze, curse, sleep)
	----------------------------------
	--Variables	
	inst.variant = "_curse"
	
	inst.hit_recovery = 0.75
	
	inst.mobsleep = true
	--inst.taunt = true
	inst.taunt2 = true
	inst._ismonster = true
	
	inst.minions = {}
	
	inst.shouldwalk = true
	inst.isangry = true
	
	inst.sounds = sounds
	inst.FireProjectile = FireProjectile
	inst.SetMinionTarget = SetMinionTarget
	
	local body_symbol = "kat_head"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	PlayablePets.SetCommonStatResistances(inst, 1, 0) --fire, acid, poison
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.fx = inst:DoPeriodicTask(0.3, function(inst)
		inst:DoTaskInTime(math.random()+1, function(inst)
			local fx = SpawnPrefab("sk_static_sparkp")
			local pos = inst:GetPosition()
			local offset = 1
			fx.Transform:SetPosition(pos.x + math.random(-offset, offset), (pos.y + (math.random(-offset, offset)/2)) + 3, pos.z + math.random(-offset, offset))
			fx.AnimState:SetMultColour((69/255)*0, (41/255)*0, (91/255)*0, 1)
			fx.AnimState:SetScale(2, 2)
			fx.AnimState:PlayAnimation(math.random(1, 4), false)
		end)
	end)
	----------------------------------
	--Light--
	inst.AnimState:SetSymbolLightOverride("mouth", 1)
	inst.AnimState:SetSymbolLightOverride("eye", 1)
	inst.glow = false
	inst.Light:Enable(false)
	inst.Light:SetRadius(3)
	inst.Light:SetFalloff(1.6)
	inst.Light:SetIntensity(0.25)
	inst.Light:SetColour(244/255, 69/255, 255/255)
	
	--[[
	local fx = SpawnPrefab("sk_enemy_shieldp") --todo, not showing up for clients
	fx.Transform:SetScale(1.25, 1.25, 1.25)
	fx.AnimState:PlayAnimation("back")
    fx.entity:SetParent(inst.entity)
	inst.fx = fx]]
	
	inst.SoundEmitter:PlaySound("sk/creatures/spookat/ambience", "ghost_loop")
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.BOOK, FOODTYPE.MEAT, FOODTYPE.GENERIC, FOODTYPE.VEGGIE }, { FOODTYPE.BOOK, FOODTYPE.MEAT, FOODTYPE.GENERIC, FOODTYPE.VEGGIE })
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	inst:SetPhysicsRadiusOverride(0.5)
	MakeFlyingCharacterPhysics(inst, 10, 0.5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2.5, 1.5)
	
	inst:DoPeriodicTask(0.3, DoCursePulse)
	--inst.DynamicShadow:Enable(false)  

	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("onremove", KillAllMinions)
	inst:ListenForEvent("death", KillAllMinions)
	inst.components.combat.onhitotherfn = OnHitOther
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)