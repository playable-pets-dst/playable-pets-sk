
local prefabs = 
{
}

local assets =
{
    Asset("ANIM", "anim/sk_graveyard_path"),
}

local function DoFadeOut(inst)
	inst.components.colourtweener:StartTween({0, 0, 0, 0}, inst.duration, inst.Remove)
end

--MakeFX(name, bank, build, anim, animloop, loop, sound, isflat)
local function MakeFX(name, data)
    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()

        inst.AnimState:SetBank(data.bank)
        inst.AnimState:SetBuild(data.build)
        inst.AnimState:PlayAnimation(data.anim)
		if data.animloop then
			inst.AnimState:PushAnimation(data.animloop, true)
		end
		if data.variations then
			inst.AnimState:PlayAnimation(math.random(1, data.variations))
		end
		
		if data.bloom then
			inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
		end

        inst:AddTag("FX")
		inst:AddTag("NOCLICK")
		
		if data.faced then
			if data.faced == 4 then
				inst.Transform:SetFourFaced()
			elseif data.faced == 6 then
				inst.Transform:SetSixFaced()
			elseif data.faced == 8 then
				inst.Transform:SetEightFaced()
			else
				inst.Transform:SetTwoFaced()
			end
		end
		
		if data.order then
			inst.AnimState:SetSortOrder(data.order)
		end
		
		if data.lightoverride then
			inst.AnimState:SetLightOverride(data.lightoverride)
		end
		
		if data.isflat then
			--ideally we want them to just be under characters and above everything else
			inst.AnimState:SetRayTestOnBB(true)
			inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
			inst.AnimState:SetLayer(LAYER_BACKGROUND)
			inst.AnimState:SetSortOrder(3)
		end

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
		
		if not data.loop then --Bad name, TODO rename this. Anything with this variable is killed outside of this.
			inst:DoTaskInTime(1, inst.Remove)
		end
		
		if data.fadeout and data.duration then
			inst.duration = data.duration
			inst:AddComponent("colourtweener")
			inst:ListenForEvent("animqueueover", DoFadeOut)
		end

        return inst
    end

    return Prefab(name, fn, assets)
end

return MakeFX("sk_graveyard_path", {bank = "sk_enemy_shield", build = "sk_enemy_shield", anim = "1", loop = true, isflat = true, variations = 4})
