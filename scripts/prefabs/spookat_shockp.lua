local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "spookat_shockp"

local assets = 
{
	Asset("ANIM", "anim/sk_spookat.zip"),
	Asset("ANIM", "anim/sk_spookat_shock.zip"),
}

local prefabs = 
{	
	"weaponsparks_fx"
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 350,
	hunger = 125,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	
	runspeed = 6*0.8,
	walkspeed = 4*0.8,
	
	attackperiod = 2,
	damage = 40*2,
	range = 10,
	hit_range = 3,
	
	bank = "sk_spookat",
	build = "sk_spookat_shock",
	shiny = "sk_spookat_shock",
	
	scale = 1.2,
	stategraph = "SGspookatp",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('spookat_nrmp',
{
    
})

local sounds =
{
	grunt = "sk/creatures/spookat/meow",
	taunt = "sk/creatures/spookat/mew",
    attack = "sk/creatures/spookat/attack",
	spit = "sk/creatures/spookat/spit",
    hit = "sk/creatures/blackkat/hit",
    death = "sk/creatures/spookat/death",
}

--==============================================
--					Mob Functions
--==============================================
local function OnHitOther(inst, other, damage, stimuli)
    if other and (not stimuli or stimuli ~= "status") then
		PlayablePets.InflictStatus(inst, other, "shock", 0.2)
	end
end

--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.variant = data.variant or nil
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.variant = inst.variant or nil
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst, forgestats)
	PlayablePets.SetForgeStats(inst, forgestats or PPSK_FORGE.BLACKKAT)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees","darts","books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("spookat")
	inst:AddTag("undead")
	inst:AddTag("monster")	
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "shadow","kat", "undead"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	end
end

local function FireProjectile(inst, target)
    if target then
		local pos = inst:GetPosition()
		local target_pos = target:GetPosition()
		local total_projectiles = 2
		local angle_between_projectiles = 10
		local angledif = 30
		for i = 0, total_projectiles do
			local angle = inst.Transform:GetRotation() + ((angledif * i) - angledif)
			local bullet = SpawnPrefab("sk_bullet_shock")
			bullet.Transform:SetPosition(pos:Get())			
			bullet.components.projectile:Throw(inst, target)
			bullet.Transform:SetRotation(angle)
			bullet.NOTAGS = GetExcludeTags(inst)
			bullet.owner = inst
			bullet.damage = mob.damage/2
		end	
    end
end

local function SetLight(inst)
	inst.AnimState:SetLightOverride(0.5)
	inst.Light:Enable(true)
	inst.Light:SetRadius(2)
	inst.Light:SetFalloff(1.6)
	inst.Light:SetIntensity(0.25)
	inst.Light:SetColour(69/255, 255/255, 202/255)
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	
	PlayablePets.SetFamily(inst, SK_FAMILIES.UNDEAD) 
	PlayablePets.SetCommonStatusResistances(inst, nil, nil, nil, nil, nil, 0) --(inst, stun, fire, shock, poison, freeze, curse, sleep)
	----------------------------------
	--Variables	
	inst.variant = ""
	
	inst.hit_recovery = 0.75
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst._ismonster = true
	
	inst.minions = {}
	
	inst.shouldwalk = true
	
	inst.sounds = sounds
	inst.FireProjectile = FireProjectile
	
	SetLight(inst)
	
	local body_symbol = "kat_head"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	PlayablePets.SetCommonStatResistances(inst, 1, 0) --fire, acid, poison
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Light--
	
	inst.SoundEmitter:PlaySound("sk/creatures/spookat/ambience", "ghost_loop")
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	--inst.components.eater:SetDiet({ FOODTYPE.BOOK, FOODTYPE.MEAT, FOODTYPE.GENERIC, FOODTYPE.VEGGIE }, { FOODTYPE.BOOK, FOODTYPE.MEAT, FOODTYPE.GENERIC, FOODTYPE.VEGGIE })
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	inst:SetPhysicsRadiusOverride(0.5)
	MakeFlyingCharacterPhysics(inst, 10, 0.5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2.5, 1.5)
	--inst.DynamicShadow:Enable(false)  

	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = OnHitOther
	--inst.components.combat.onhitotherfn = OnHitOther
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true)
			SetLight(inst)
		end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)