local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "bombie_dustp"

local assets = 
{
	Asset("ANIM", "anim/sk_zombie.zip"),
	Asset("ANIM", "anim/sk_bombie_dust.zip"),
	--Asset("ANIM", "anim/sk_zombie_freeze.zip"),
	--Asset("ANIM", "anim/sk_zombie_shock.zip"),
	--Asset("ANIM", "anim/sk_zombie_poison.zip"),
	--Asset("ANIM", "anim/sk_zombie_fire.zip"),
	--Asset("ANIM", "anim/sk_zombie_curse.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 250,
	hunger = 100,
	hungerrate = 0, 
	sanity = 50,
	
	runspeed = 10,
	walkspeed = 3,
	
	attackperiod = 2,
	damage = 200,
	range = 3,
	hit_range = 4.25,
	
	bank = "sk_zombie",
	build = "sk_bombie_dust",
	shiny = "sk_bombie_dust",
	
	scale = 0.9,
	stategraph = "SGzombiep",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('bombie_dustp',
{
    {"boneshard",       1.00},
    {"boneshard",   	1.00},
})

local sounds =
{
	grunt = "sk/creatures/bombie/grunt",
    attack = "sk/creatures/bombie/attack",
	attack2 = "sk/creatures/bombie/attack",
    hit = "sk/creatures/bombie/hit",
    death = "sk/common/hit/hit_zombie",
    spawn = "sk/creatures/zombie/spawn",
	explode = "sk/creatures/bombie/explode",
	fuse = "sk/creatures/bombie/fuse_loop"
}

--==============================================
--					Mob Functions
--==============================================

--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.variant = data.variant or nil
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.variant = inst.variant or nil
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst, forgestats)
	PlayablePets.SetForgeStats(inst, forgestats or PPSK_FORGE.ZOMBIE)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees","darts","books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("zombie")
	inst:AddTag("bombie")
	inst:AddTag("monster")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local function OnAttackOther(inst, other, damage, stimuli)
    if other and (not stimuli or stimuli ~= "status") then
		PlayablePets.InflictStatus(inst, other,"stun", 0.5)
	end
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 2, 2) --fire, acid, poison
	
	PlayablePets.SetFamily(inst, SK_FAMILIES.UNDEAD) --inst, prefaboverride
	--PlayablePets.SetCommonStatusResistances() --(inst, stun, fire, shock, poison, freeze, curse, sleep)
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt2 = true
	
	inst.hit_recovery = 0.75
	
	inst.variant = ""
	
	inst.shouldwalk = true
	
	inst.altattack = true
	
	inst.sounds = sounds
	
	inst.getskins = getskins
	
	local body_symbol = "swap_hat"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(2,2,2) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	inst:SetPhysicsRadiusOverride(0.5)
	MakeCharacterPhysics(inst, 10, 0.5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2.5, 1.5)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = OnAttackOther
	--inst:ListenForEvent("killed", PlayablePets.ZombieOnKill)
	
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)