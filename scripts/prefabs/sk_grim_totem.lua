--require "scripts/sk_tuning"
--TODO clean this shit up!
local assets =
{
    Asset("ANIM", "anim/sk_grim_totem.zip"),
}

local prefabs =
{
    
}

local sounds =
{
	
}

SetSharedLootTable('sk_grim_totem',
{
    {"nightmarefuel",        1.00},
	{"nightmarefuel",        0.50},
    {"rocks",   			 1.00},
	{"rocks",   			 1.00},
	{"rocks",   			 1.00},
})

SetSharedLootTable('sk_grem_totem',
{
    {"gears",        1.00},
	{"gears",        1.00},
	{"gears",        0.50},
})

local function OnSave(inst, data)

end

local function OnLoad(inst, data)

end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddLight()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 1)

    inst.DynamicShadow:SetSize(2.5, 1.5)
    --inst.Transform:SetOneFaced()
	local scale = 1
	inst.Transform:SetScale(scale, scale, scale)

    inst:AddTag("structure")
	inst:AddTag("grim_totem")
	inst:AddTag("NOCLICK")

    inst.AnimState:SetBank("sk_grim_totem")
    inst.AnimState:SetBuild("sk_grim_totem")
    inst.AnimState:PlayAnimation("idle", true)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("inspectable")
	
	--local fx = SpawnPrefab("sk_rez_ringp")
	---fx.Transform:SetScale(1.75, 1.75, 1.75)
	--fx.AnimState:PlayAnimation("spawn", false)
    --fx.entity:SetParent(inst.entity)
	--inst.circle = fx
	
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst:AddComponent("attack_radius_display")
		inst.components.attack_radius_display:AddCircle("rez", 12, WEBCOLOURS.RED)
		inst.components.attack_radius_display:AddCircle("rezbase", 1, WEBCOLOURS.RED)
	end
	
	inst:AddComponent("sk_rezaura")
	inst:DoPeriodicTask(5, function(inst) inst.components.sk_rezaura:DoPulse() end)
	

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

return Prefab("sk_grim_totem", fn, assets, prefabs)
