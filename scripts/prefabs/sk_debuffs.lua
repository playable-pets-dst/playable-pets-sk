
local function GetResistance(inst, target)
	local type = inst.type
	
	if type == "poison" then
		return target.poison_resist and 1 * target.poison_resist or 1
	elseif type == "stun" then
		return target.stun_resist and 1 * target.stun_resist or 1
	elseif type == "fire" then
		return target.fire_resist and 1 * target.fire_resist or 1
	elseif type == "freeze" then
		return target.freeze_resist and 1 * target.freeze_resist or 1	
	elseif type == "shock" then
		return target.shock_resist and 1 * target.shock_resist or 1
	elseif type == "curse" then
		return target.curse_resist and 1 * target.curse_resist or 1
	elseif type == "sleep" then
		return target.sleep_resist and 1 * target.sleep_resist or 1
	end
end

local function OnChangeFollowSymbol(inst, target, followsymbol, followoffset)
	--print("DEBUG: OnChangeFollowSymbol ran!")
    inst.Follower:FollowSymbol(target.GUID, followsymbol or "head", followoffset.x, followoffset.y, followoffset.z)
end

------------------------------------------------
local function OnAttached_Poison(inst, target)
	local fx = SpawnPrefab("sk_poison_fxp")
	fx.entity:SetParent(target.entity)
	inst.fx = fx
	inst.SoundEmitter:PlaySound("sk/common/status/poison")
	if target and target.components.combat and target.components.health and not target.atkdebuffed and not target.defbuffed then
		target.defdebuffed = true
		target.atkdebuffed = true
		local olddmgmult = target.components.combat.damagemultiplier or nil
		inst.olddmgmult = olddmgmult
		if TheNet:GetServerGameMode() == "lavaarena" then
			target.components.combat:AddDamageBuff("sk_poison_debuff_atk", 0.5, false)
			target.components.combat:AddDamageBuff("sk_poison_debuff_def", 0.25, true)
		else
			target.components.combat.damagemultiplier = 0.5
			target.components.health:SetAbsorptionAmount(-0.75)
		end
		
		inst.healmult = target.healmult or nil
		target.healmult = 0
		if target.components.eater then
			local oldeater = target.components.eater.healthabsorption or 1
			inst.oldeater = oldeater
			target.components.eater.healthabsorption = 0
		end
	end
end

local function OnDetached_Poison(inst, target)
	if inst.fx then
		inst.fx:Remove()
	end
	if target and target.components.combat and target.components.health then
		target.defdebuffed = nil
		target.atkdebuffed = nil
		target.components.combat.damagemultiplier = inst.olddmgmult
		if TheNet:GetServerGameMode() == "lavaarena" then
			target.components.combat:RemoveDamageBuff("sk_poison_debuff_atk", 0.5, false)
			target.components.combat:RemoveDamageBuff("sk_poison_debuff_def", 0.25, true)
		else
			target.components.health:SetAbsorptionAmount(0)
			target.components.combat.damagemultiplier = 1
		end
		target.healmult = inst.healmult
		if target.components.eater then
			target.components.eater.healthabsorption = inst.oldeater
		end
	end
end
-------------------
local function CurseDamage(inst)
	if inst.components.health then
		local pos = inst:GetPosition()
		local damage = math.min(inst.components.health.maxhealth/TUNING.SK.CURSE.DAMAGE_PERCENT, TUNING.SK.CURSE.DAMAGE_CAP)
		SpawnPrefab("sk_curse_hitp").Transform:SetPosition(pos.x, pos.y + 2, pos.z)
		inst.SoundEmitter:PlaySound("sk/common/hit/sk_hit")
		if inst.curse_inflicter and not inst.mobplayer then --mobplayer check is to avoid stunlocking on wilson
			inst.components.combat:GetAttacked(inst.curse_inflicter or nil, damage, nil, "status")
		else
			inst.components.health:DoDelta(-damage, false, nil, nil, nil, true)
		end
		
		if inst.components.colourtweener and not inst:HasTag("shadow") then
			local oldcolor = inst.AnimState:GetMultColour()
			inst.AnimState:SetMultColour(252/255, 3/255, 177/255,1)
			inst.components.colourtweener:StartTween({oldcolor}, 0.5)
		end
	end
end

local function OnAttached_Curse(inst, target)
	inst.SoundEmitter:PlaySound("sk/common/status/curse")
	if target.components.colourtweener and not target:HasTag("shadow") then
		local oldcolor = target.AnimState:GetMultColour()
		target.AnimState:SetMultColour(252/255, 3/255, 177/255,1)
		target.components.colourtweener:StartTween({oldcolor}, 1)
	end
	local fx = SpawnPrefab("sk_curse_fxp")
	if target:HasTag("epic") then
		fx.AnimState:SetScale(4, 4)
	else
		fx.AnimState:SetScale(2, 2)
	end
	fx.Transform:SetPosition(target:GetPosition():Get())
	
	if inst.inflicter then
		target.curse_inflicter = inst.inflicter
	end
	
	if target and target.components.combat and target.components.health then
		target:ListenForEvent("cursed_attack", CurseDamage)
	end
end

local function OnDetached_Curse(inst, target)
	if target and target.components.combat and target.components.health then
		target:RemoveEventCallback("cursed_attack", CurseDamage)
		if target.components.colourtweener and not target:HasTag("shadow") then
			--inst.AnimState:SetMultColour(1, 1, 1, 1)
		end
		if target.curse_afflicter then
			target.curse_afflicter = nil
		end
	end
end
------------------------------------------------
local function DoShockTick(inst)
	SpawnPrefab("sk_shock_fxp").Transform:SetPosition(inst:GetPosition():Get())
	if inst.SoundEmitter then
		inst.SoundEmitter:PlaySound("sk/common/status/shock_pulse")
	end
	if inst.components.health and inst.sg then
		if inst.sg.sg.states.stun then
			inst.sg:GoToState("stun", {stimuli = "electric"})
		else
			inst.sg:GoToState(inst.sg.sg.states.electrocute and "electrocute" or "hit")
		end
	end
	inst.components.health:DoDelta(-10, false, nil, nil, nil, true)
end

local function OnAttached_Shock(inst, target, followsymbol, followoffset)
	if not target:HasTag("epic") then
		inst.AnimState:SetScale(-1, -1)
	else
		inst.AnimState:SetScale(-2, -2)
	end
	if inst.inflicter then
		target.shock_inflicter = inst.inflicter
	end
	inst.AnimState:SetMultColour(161/255, 251/255, 213/255, 1)
	inst.SoundEmitter:PlaySound("sk/common/status/shock")
	OnChangeFollowSymbol(inst, target, followsymbol, followoffset)
	if target and target.components.combat and target.components.health then
		DoShockTick(target)
		target.sk_shocktask = target:DoPeriodicTask(math.random(2, 3), DoShockTick)
	end
end

local function OnDetached_Shock(inst, target)
	if target and target.components.combat and target.components.health then
		if target.sk_shocktask then
			target.sk_shocktask:Cancel()
			target.sk_shocktask = nil
		end
		
		if inst.shock_inflicter then
			target.shock_inflicter = nil
		end
	end
end
------------------------------------------------
local function DoFireTick(inst)
	if inst.SoundEmitter then
		inst.SoundEmitter:PlaySound("sk/common/status/fire_pulse")
	end
	inst.components.health:DoDelta(inst:HasTag("epic") and -50 or -20, false, nil, nil, nil, true)
	if inst.components.colourtweener and not inst:HasTag("shadow") then
		local oldcolor = inst.AnimState:GetMultColour()
		inst.AnimState:SetMultColour(255/255, 89/255, 0/255,1)
		inst.components.colourtweener:StartTween({oldcolor}, 0.5)
	end
end

local function OnAttached_Fire(inst, target, followsymbol, followoffset)
	local fx = SpawnPrefab("sk_fire_fxp")
	fx.entity:SetParent(target.entity)
	inst.fx = fx
	inst.SoundEmitter:PlaySound("sk/common/status/fire")
	if inst.inflicter then
		target.fire_inflicter = inst.inflicter
	end
	if target and target.components.combat and target.components.health then
		target.sk_firetask = target:DoPeriodicTask(math.random(2.5), DoFireTick)
	end
end

local function OnDetached_Fire(inst, target)
	if inst.fx then
		inst.fx:Remove()
	end
	if inst.fire_inflicter then
		target.fire_inflicter = nil
	end
	if target.sk_firetask then
		target.sk_firetask:Cancel()
		target.sk_firetask = nil
	end
end
------------------------------------------------
local function OnAttached_Stun(inst, target)
	inst.SoundEmitter:PlaySound("sk/common/status/stun")
	if target and target.components.combat and target.components.health  and target.components.locomotor then
		--TODO: find a way to delay timeevents in stategraphs too?
		target.AnimState:SetDeltaTimeMultiplier(0.5)
		target.components.locomotor.externalspeedmultiplier = 0.5
	end
end

local function OnDetached_Stun(inst, target)
	if target and target.components.combat and target.components.health and target.components.locomotor then
		target.AnimState:SetDeltaTimeMultiplier(1)
		target.components.locomotor.externalspeedmultiplier = 1
	end
end
------------------------------------------------
local function OnAttached_Sleep(inst, target)
	inst.SoundEmitter:PlaySound("sk/common/status/sleep")
	if target and target.components.combat and target.components.health and target.components.sleeper then
		--TODO: find a way to delay timeevents in stategraphs too?
		target.components.sleeper.tiredness = 9000
	end
end

local function OnDetached_Sleep(inst, target)
	if target and target.components.combat and target.components.health then
		target.components.sleeper.tiredness = 0
	end
end
------------------------------------------------
local function OnAttached_Freeze(inst, target)
	inst.SoundEmitter:PlaySound("sk/common/status/freeze")
	if target and target.components.combat and target.components.health and target.components.sleeper then
		--TODO: find a way to delay timeevents in stategraphs too?
		target.components.sleeper.tiredness = 9000
	end
end

local function OnDetached_Freeze(inst, target)
	if target and target.components.combat and target.components.health then
		target.components.sleeper.tiredness = 0
	end
end
------------------------------------------------


local function OnAttached(inst, target, followsymbol, followoffset)
	local text = SpawnPrefab("sk_status_textp")
	text.Transform:SetPosition(target:GetPosition():Get())
	text.AnimState:OverrideSymbol("stun", "status_text", inst.type)
	
    inst.entity:SetParent(target.entity)
	inst.entity:AddFollower()
	OnChangeFollowSymbol(inst, target, followsymbol, followoffset)
	inst.components.timer:StartTimer(inst.prefab.."over", inst.duration * GetResistance(inst, target))
    inst:ListenForEvent("death", function()
        inst.components.debuff:OnDetach()
    end, target)
	if inst.attachfn then
		inst.attachfn(inst, target, followsymbol, followoffset)
	end
end

local function OnDetached(inst, target)	
	if inst.detachfn then
		inst.detachfn(inst, target)
	end
	if target.has_debuff and target.has_debuff[inst.prefab] then
		target.has_debuff[inst.prefab] = nil
	end
	
	inst:Remove()
end

local function OnTimerDone(inst, data)
    if data.name == inst.prefab.."over" then
        inst.components.debuff:OnDetach()
    end
end

local function OnExtended(inst, target)
    inst.components.timer:StopTimer(inst.prefab.."over")
    inst.components.timer:StartTimer(inst.prefab.."over", inst.duration)
end

local function MakeBuff(name, type, duration, attachfn, detachfn, bank, build, anim_pre, anim, loop, scale)
	
	local function fn()
		local inst = CreateEntity()
		local trans = inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddNetwork()
		inst.entity:AddSoundEmitter()
		
		inst.entity:SetPristine()
		
		inst.AnimState:SetBank(bank)
		inst.AnimState:SetBuild(build)
		if anim_pre and not anim then
			inst.AnimState:PlayAnimation(anim_pre)
		else
			inst.AnimState:PlayAnimation(anim_pre)
			inst.AnimState:PushAnimation(anim, loop or false)
		end
		if scale then
			inst.AnimState:SetScale(scale, scale)
		end

		if not TheWorld.ismastersim then
			return inst
		end
		
		inst.attachfn = attachfn
		inst.detachfn = detachfn
		inst.duration = duration or 8
		inst.type = type
		
		inst:AddComponent("timer")
		inst:ListenForEvent("timerdone", OnTimerDone)
		
		inst:AddComponent("debuff")
		inst.components.debuff:SetAttachedFn(OnAttached)
		inst.components.debuff:SetDetachedFn(OnDetached)
		inst.components.debuff:SetExtendedFn(OnExtended)
		--inst.components.debuff.keepondespawn = true

		
		return inst
	end

	return Prefab(name, fn)
end

------------------------------------------------------------------------------
--TODO make tuning values for the debuffs
return MakeBuff("sk_curse_debuff", "curse", 30, OnAttached_Curse, OnDetached_Curse, "status_curse_fx", "status_curse_fx", "idle", "idle", true, 2),
MakeBuff("sk_shock_debuff", "shock", 8, OnAttached_Shock, OnDetached_Shock, "mossling_spin_fx", "mossling_spin_fx", "spin_loop", "spin_loop", true, 2),
MakeBuff("sk_fire_debuff", "fire", 8, OnAttached_Fire, OnDetached_Fire, "mossling_spin_fx", "mossling_spin_fx", "lol", "no", true),
MakeBuff("sk_poison_debuff", "poison", 8, OnAttached_Poison, OnDetached_Poison, "mossling_spin_fx", "mossling_spin_fx", "lol", "no", true),
MakeBuff("sk_stun_debuff", "stun", 6, OnAttached_Stun, OnDetached_Stun, "mossling_spin_fx", "mossling_spin_fx", "lol", "no", true)
--all timers beside stun is 8