local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "grimalkinp"

local assets = 
{
	Asset("ANIM", "anim/sk_grimalkin.zip"),
	Asset("ANIM", "anim/sk_grimalkin_nrm.zip"),
}

local prefabs = 
{	
	"weaponsparks_fx"
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.GRIMALKINP_HEALTH,
	hunger = TUNING.GRIMALKINP_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.GRIMALKINP_SANITY,
	
	runspeed = 6.5/2.5,
	walkspeed = 6.5/2.5,
	
	attackperiod = 2,
	damage = 90*2,
	range = 3,
	hit_range = 4,
	
	bank = "sk_grimalkin",
	build = "sk_grimalkin_nrm",
	shiny = "sk_grimalkin",
	
	scale = 2.5,
	stategraph = "SGgrimalkinp",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('grimalkinp',
{

})

local sounds =
{
	--grunt = "sk/creatures/grimalkin/growl",
	moan = "sk/creatures/grimalkin/moan",
    attack = "sk/creatures/grimalkin/bite",
	spawn = "sk/creatures/grimalkin/spawn",
}
--==============================================
--					Mob Functions
--==============================================
local function OnHitOther(inst, other, damage, stimuli)
    if other and (not stimuli or stimuli ~= "status") then
		if SK_CURSE_ENABLE then
			PlayablePets.InflictStatus(inst, other, "curse", 0.1)
		end
	end
end

--==============================================
--				Custom Common Functions
--==============================================
local function OnPhaseChanged(inst, phase)
	if TheWorld.state.isday then
		PlayablePets.StopPassiveHeal(inst)
		inst.components.health:StartRegen(-3, 0.25)
	else
		PlayablePets.SetPassiveHeal(inst)
		inst.components.health:StopRegen()
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.variant = data.variant or nil
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.variant = inst.variant or nil
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst, forgestats)
	PlayablePets.SetForgeStats(inst, forgestats or PPSK_FORGE.BLACKKAT)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees","darts","books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	----------------------------------
	--Tags--
	inst:AddTag("grimalkin")
	inst:AddTag("undead")
	inst:AddTag("largecreature")
	inst:AddTag("monster")	
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetFamily(inst, SK_FAMILIES.UNDEAD) 
	PlayablePets.SetCommonStatusResistances(inst, 0, 0, 0, 0, 0, 0) --(inst, stun, fire, shock, poison, freeze, curse, sleep)
	PlayablePets.SetPassiveHeal(inst)
	----------------------------------
	--Variables	
	inst.variant = ""
	
	inst.hit_recovery = 0.75
	
	inst.specialsleep = true
	
	inst.shouldwalk = true
	
	inst.sounds = sounds
	
	local body_symbol = "swap_debuff"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Light--
	inst.glow = false
	inst.Light:Enable(false)
	inst.Light:SetRadius(3)
	inst.Light:SetFalloff(1.6)
	inst.Light:SetIntensity(0.25)
	inst.Light:SetColour(244/255, 69/255, 255/255)
	
	--inst.SoundEmitter:PlaySound("sk/creatures/spookat/ambience", "ghost_loop")
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.BOOK, FOODTYPE.MEAT, FOODTYPE.GENERIC, FOODTYPE.VEGGIE }, { FOODTYPE.BOOK, FOODTYPE.MEAT, FOODTYPE.GENERIC, FOODTYPE.VEGGIE })
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	inst:SetPhysicsRadiusOverride(0.5)
	MakeFlyingCharacterPhysics(inst, 10, 0.5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2.5, 1.5)
	inst.DynamicShadow:Enable(false)  

	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:WatchWorldState("isday", OnPhaseChanged)
  	inst:WatchWorldState("isdusk", OnPhaseChanged)
	OnPhaseChanged(inst)
	--inst.components.combat.onhitotherfn = OnHitOther
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) OnPhaseChanged(inst) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)