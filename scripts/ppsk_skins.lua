local SKINS = {
	--[[
	prefab = {
		skinname = {
			name = "", --this is for the purpose of printing
			fname = "Name of Skin",
			build = "build name",
			teen_build = "",
			adult_build = "adult build name",
			etc_build = "blah blah build", --add as many as these as you need
			fn = functionhere,
			owners = {}, --userids go here
			locked = true, --this skin can't be used.
		},
	},
	]]
	mewkatp = {
		fancy = {
			name = "fancy",
			fname = "Moorcraft Kat",
			build = "sk_spookat_moorcraft",
			scale = 2,
		}
	},
	zombie_dustp = {
		bellhop = {
			name = "bellhop",
			fname = "Zombie Bellhop",
			build = "sk_zombie_bellhop",
		},
		swarm = {
			name = "swarm",
			fname = "Void Zombie",
			build = "sk_zombie_swarm",
		}
	},
}

return SKINS