/////////////////////////////////////
-----Spiral Knights Health Notes-----
/////////////////////////////////////

T1 base health is always 40. There are classes that add multipliers to that, for example:

Lumbers are of the +10 class which returns a multiplier of 2 to the base value.

Theres plenty of multipliers so some maybe used or not.

Tier 4 End = 2160

Tier 3 End = 1080

Tier 3 Mid = 675

Tier 3 Start = 450

Tier 2 End = 375

Tier 2 Start = 160

Tier 1 End = 150

Tier 1 Start = 40

