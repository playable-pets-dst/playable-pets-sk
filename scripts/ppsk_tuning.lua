----------------------------------------------------------
--Tuning
----------------------------------------------------------
TUNING.GRIMALKIN_CURSEP_HEALTH = 4000
TUNING.GRIMALKIN_CURSEP_HUNGER = 250
TUNING.GRIMALKIN_CURSEP_SANITY = 150

TUNING.GRIMALKINP_HEALTH = 2500
TUNING.GRIMALKINP_HUNGER = 200
TUNING.GRIMALKINP_SANITY = 100
----------------------------------------------------------
--Non-Player Tuning
----------------------------------------------------------
TUNING.SK = 
{
	CLASSES =
	{
		MINION = 0.5,
		BASE = 1,
		ELITE = 1.5,
		GUARDIAN = 2,
		BOSS = 11,	--TODO confirm
		BOSS2 = 14,
		BOSS3 = 18
	},

	BULLET = 
	{
		DURATION = 10,
		HIT_DIST = 1,
		SPEED = 10,
		RANGE = 50,
		LAUNCH_OFFSET = 1,
		DAMAGE = 30,
		STATUS_CHANCE = 0.3,
	},
	
	GRIMALKIN = {
		HEALTH = 1,
		DAMAGE = 75,
		ATTACK_RANGE = 4,
		ATTACK_PERIOD = 3,
		MAX_SPEED = 2,
		TARGET_RANGE = 30,
		
		BITE_OFFSET = 2.5,
		AOE_RANGE = 4,
	},
	
	MARGREL = {
		HEALTH = 35000,
		DAMAGE = 100,
		ATTACK_RANGE = 4,
		ATTACK_PERIOD = 3,
		MAX_SPEED = 2,
		TARGET_RANGE = 40,
		
		BITE_OFFSET = 2.5,
		AOE_RANGE = 4,
		
		BULLET_RATE = 5,
		BULLET_DURATION = 6, 
		BULLET_DAMAGE = 30,
		BULLET_SPEED = 5,
		BULLET_SPAWN_RANGE = 15,
		BULLET_LIFE = 10,
		
		SHIELD_TIME = 7,
		ATTACK_TIME = 3, --after shield breaks
	},
	
	CURSE = {
		DURATION = 30,
		DAMAGE_CAP = 150,
		DAMAGE_PERCENT = 10,
	},
	
	STUN = {
		DURATION = 6,
		SPEED_REDUCTION = 0.5,		
	},
	
	FIRE = {
		DURATION = 8,
		DAMAGE = 30,
		TICK = 0.5,
	},
	
	SHOCK = {
		DURATION = 8,
		DAMAGE = 10,		
		TICK_MIN = 0.5,
		TICK_MAX = 1,
	},
	
	FREEZE = {
		DURATION = 8,
		DAMAGE = 20,		
	},
	
	CURSE_AURA_CHANCE = 0.04,
}


TUNING.SK_ZOMBIES = {
	HEALTH = 750,
	DAMAGE = 50,
	ATTACK_PERIOD = 3,
	ATTACK_RANGE = 8,
	ATTACK_MELEE_RANGE = 2,
	HIT_RANGE = 3,
	WALK_SPEED = 3,
	RUN_SPEED = 9, --used by bombies only
	HIT_RECOVERY = 0.75,
	BREATH_TIME = 4,
	REVIVE_WAIT_TIME = 15,
	ALTATTACK_COOLDOWN = 10,
	
	LIGHT_RADIUS = 3,
	LIGHT_FALLOFF = 1.6,
	LIGHT_INTENSITY = 0.25,
	
	BREATH_FOG_TICK = 0.5,
	BREATH_FOG_OFFSET = 1,
	BREATH_FOG_Y_OFFSET = 2,
	
	BREATH_PARTICLE_TICK = 0.25,
	BREATH_PARTICLE_ANGLEDIF = 45,
	BREATH_PARTICLE_CHANCE = 0.2,
	BREATH_PARTICLE_Y_OFFSET = 2,
	BREATH_PARTICLE_OFFSET = 1,
	
	BREATH_AOE_OFFSET = 3,
	BREATH_AOE_RANGE = 4.5,
	BREATH_COOLDOWN = 12,
	
	TARGET_RANGE = 20,
	FOLLOWER_RETURN_DISTANCE = 15,
	TARGET_TIME = 30,
	
	INFLICT_CHANCE = 0.2,
	
	--Bombies
	EXPLODE_DAMAGE = 200,
	EXPLODE_ATTACK_RANGE = 2,
	EXPLODE_HIT_RANGE = 4,
	EXPLODE_INFLICT_CHANCE = 0.5,
}

TUNING.SK_SPOOKATS = {
	HEALTH = 500,
	DAMAGE = 45,
	ATTACK_PERIOD = 4,
	ATTACK_RANGE = 8,
	ATTACK_MELEE_RANGE = 3,
	HIT_RANGE = 3,
	WALK_SPEED = 3,
	RUN_SPEED = 5, 
	HIT_RECOVERY = 0.75,
	
	LIGHT_RADIUS = 3,
	LIGHT_FALLOFF = 1.6,
	LIGHT_INTENSITY = 0.25,
	
	TARGET_RANGE = 10,
	FOLLOWER_RETURN_DISTANCE = 7,
	TARGET_TIME = 10,
	
	INFLICT_CHANCE = 0.2,

	MAX_MINIONS = 8,
	
	--Bullets--
	ANGLE_DIF = 30,
	NUM_PROJ = 2, --these are extra bullets
	BULLET_DAMAGE = 30,
	BULLET_SPEED = 5,
	
	SCALE = 1.2,
}

TUNING.SK_BLACKKAT = {
	HEALTH = TUNING.SK_SPOOKATS.HEALTH * 3,
	DAMAGE = TUNING.SK_SPOOKATS.DAMAGE * 2,
	SUMMON_TICK = 0.5,
	SUMMON_COOLDOWN = 20,
}
----------------------------------------------------------
--Reforged Tuning
----------------------------------------------------------
--difficulties are numerical, from 1 to 3 or more.
TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.ZOMBIE_DUSTP = {"petrifyingtome", "forge_woodarmor"}
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.ZOMBIE_DUSTP= 2
TUNING.LAVAARENA_STARTING_HEALTH.ZOMBIE_DUSTP = 300

TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.ZOMBIE_FIREP = {"forge_woodarmor"}
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.ZOMBIE_FIREP= 2
TUNING.LAVAARENA_STARTING_HEALTH.ZOMBIE_FIREP = 300

TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.ZOMBIE_FREEZEP = {"forge_woodarmor"}
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.ZOMBIE_FREEZEP= 2
TUNING.LAVAARENA_STARTING_HEALTH.ZOMBIE_FREEZEP = 300

TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.ZOMBIE_CURSEP = {"forge_woodarmor"}
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.ZOMBIE_CURSEP= 2
TUNING.LAVAARENA_STARTING_HEALTH.ZOMBIE_CURSEP = 300

TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.ZOMBIE_POISONP = {"forge_woodarmor"}
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.ZOMBIE_POISONP= 2
TUNING.LAVAARENA_STARTING_HEALTH.ZOMBIE_POISONP = 300

TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.ZOMBIE_SHOCKP = {"forge_woodarmor"}
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.ZOMBIE_SHOCKP= 2
TUNING.LAVAARENA_STARTING_HEALTH.ZOMBIE_SHOCKP = 300

TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.BOMBIE_DUSTP = {"reedtunic"}
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.BOMBIE_DUSTP= 3
TUNING.LAVAARENA_STARTING_HEALTH.BOMBIE_DUSTP = 125

TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.BOMBIE_FIREP = {"reedtunic"}
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.BOMBIE_FIREP= 3
TUNING.LAVAARENA_STARTING_HEALTH.BOMBIE_FIREP = 125

TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.BOMBIE_FREEZEP = {"reedtunic"}
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.BOMBIE_FREEZEP= 3
TUNING.LAVAARENA_STARTING_HEALTH.BOMBIE_FREEZEP = 125

--TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.BOMBIE_CURSEP = {"reedtunic"}
--TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.BOMBIE_CURSEP= 3
--TUNING.LAVAARENA_STARTING_HEALTH.BOMBIE_CURSEP = 125

TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.BOMBIE_POISONP = {"reedtunic"}
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.BOMBIE_POISONP= 3
TUNING.LAVAARENA_STARTING_HEALTH.BOMBIE_POISONP = 125

TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA.BOMBIE_SHOCKP = {"reedtunic"}
TUNING.LAVAARENA_SURVIVOR_DIFFICULTY.BOMBIE_SHOCKP= 3
TUNING.LAVAARENA_STARTING_HEALTH.BOMBIE_SHOCKP = 125

---Buffs
--TUNING.FORGE.BUFFS_DATA.WEEVOLE_SWARM_BUFF = {SYMBOL = "WATHGRITHR", BUFF = true}
--TUNING.FORGE.BUFFS_DATA.GUARD_BUFF = {SYMBOL = "WATHGRITHR", BUFF = true}
if TUNING.FORGE then
	TUNING.FORGE.BUFFS_DATA.SK_FIRE_DEBUFF = {SYMBOL = "", BUFF = false}
	TUNING.FORGE.BUFFS_DATA.SK_POISON_DEBUFF = {SYMBOL = "", BUFF = false}
	TUNING.FORGE.BUFFS_DATA.SK_FREEZE_DEBUFF = {SYMBOL = "", BUFF = false}
	TUNING.FORGE.BUFFS_DATA.SK_CURSE_DEBUFF = {SYMBOL = "", BUFF = false}
	TUNING.FORGE.BUFFS_DATA.SK_STUN_DEBUFF = {SYMBOL = "", BUFF = false}
	TUNING.FORGE.BUFFS_DATA.SK_SHOCK_DEBUFF = {SYMBOL = "", BUFF = false}
end
--------------------------------------------------------
--					SK TUNING
--------------------------------------------------------





return TUNING