require("stategraphs/commonstates")
require("stategraphs/ppstates")

local MAX_SPEED = 10 -- TODO adjust?
local MAX_DASH_TIME = 0.6 -- seconds?
local ACCELERATION = MAX_SPEED/12 -- TODO how was this determined?

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "spookat", "undead", "zombie", "grimalkin", "wall"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "shadow","spookat", "zombie", "undead", "grimalkin", "wall"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "shadow", "spookat", "zombie", "undead", "grimalkin", "wall"}
	end
end

function TableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function SetSleeperAwakeState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:RemoveImmunity("sleeping")
    end
	if inst.components.shedder ~= nil then
	inst.components.shedder:StartShedding(360)
	end
	
    if inst.components.talker ~= nil then
        inst.components.talker:StopIgnoringAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Enable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(true)
        inst.components.playercontroller:Enable(true)
    end
    inst:OnWakeUp()
    inst.components.inventory:Show()
    inst:ShowActions(true)
	--inst.sg:GoToState("taunt")
end

local function SetSleeperSleepState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:AddImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:IgnoreAll("sleeping")
    end
	
	if inst.components.shedder ~= nil then
	inst.components.shedder:StopShedding()
	end
	
    if inst.components.firebug ~= nil then
        inst.components.firebug:Disable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(false)
        inst.components.playercontroller:Enable(false)
    end
    inst:OnSleepIn()
    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")
    inst:ShowActions(false)
end

local function CalcChompSpeed(inst, target)
	--local target = inst.components.combat.target
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (target.Physics ~= nil and inst.Physics:GetRadius() + target.Physics:GetRadius() or inst.Physics:GetRadius())
        if dist > 0 then
            return inst:HasTag("blackkat") and math.min(8, dist) / (28 * FRAMES) or math.min(8, dist) / (17 * FRAMES)
        end
    end
    return 0
end

local function ExplosionShake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .03, 1, inst, 20)
end

local events=
{
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("attacked", function(inst) 
        if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("parrying") then 
            if not inst.sg:HasStateTag("attack") and not inst.isblocked and (inst.sg.mem.last_hit_time or 0) + inst.hit_recovery*2 < GetTime() and not inst.sg:HasStateTag("nointerrupt") then -- don't interrupt attack or exit shield
                inst.sg:GoToState("hit") -- can still attack
            end
			inst.isblocked = nil
        end 
    end),
    EventHandler("doattack", function(inst, data) 
        if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then 
            inst.sg:GoToState("attack", data.target) 
        end 
    end),
    EventHandler("death", function(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end

        if (data ~= nil and data.cause == "file_load") or inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
			if inst:HasTag("_isinrez") then
				inst.sg:GoToState("fake_death")
			else
				inst.sg:GoToState("death")
			end
        end
    end),
	EventHandler("respawnfromcorpse", function(inst, reviver) 
		if inst.sg:HasStateTag("death") then 
			inst.sg:GoToState("corpse_rebirth") 
		end 
	end),	
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(),
    EventHandler("entershield", function(inst) inst.sg:GoToState("shield") end),
    EventHandler("exitshield", function(inst) inst.sg:GoToState("shield_end") end),
    
	
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end

local function GetAnimation(inst, str)
	return inst.isangry and str.."_angry" or str
end

local function SetShield(inst, shield)
	if shield then
		inst.AnimState:SetMultColour(255, 50, 50, 1)
	else
		inst.AnimState:SetMultColour(1, 1, 1, 1)
	end	
end

local function SetInvisible(inst)
	inst.noactions = true
	inst.components.health:SetInvincible(true)
	inst:AddTag("notarget")
	inst:AddTag("noplayerindicator")
	inst.MiniMapEntity:SetIcon("")
	inst.components.locomotor:SetExternalSpeedMultiplier(inst, 1.5, 1.5)
	inst.components.talker:IgnoreAll("hiding")
	inst:Hide()
end

local function SetVisible(inst)
	inst.noactions = nil
	inst.components.health:SetInvincible(false)
	inst:RemoveTag("notarget")
	inst:RemoveTag("noplayerindicator")
	inst.MiniMapEntity:SetIcon(inst.mob_table.minimap)
	inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, 1.5)
	inst.components.talker:StopIgnoringAll("hiding")
	inst:Show()
end

local function DoBiteAoe(inst)
	local posx, posy, posz = inst.Transform:GetWorldPosition()
	local angle = -inst.Transform:GetRotation() * DEGREES
	local offset = 3
	local targetpos = {x = posx + (offset * math.cos(angle)), y = 0, z = posz + (offset * math.sin(angle))} 
	local ents = TheSim:FindEntities(targetpos.x, 0, targetpos.z, inst.mob_table.hit_range, {"_combat"}, GetExcludeTags(inst))
	for _,ent in ipairs(ents) do
		if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health and ent ~= inst.components.combat.target then
			inst:PushEvent("onareaattackother", { target = ent--[[, weapon = self.inst, stimuli = self.stimuli]] })
			ent.components.combat:GetAttacked(inst, 1)
			ent.components.health:DoDelta(-inst.components.combat:CalcDamage(ent)+1, false, nil, nil, inst, true) --this is to bypass armor
		end
	end
end

local function ToggleCurseAura(inst)
	if inst.nocurse then
		inst.nocurse = false
	else
		inst.nocurse = true
	end
	PlayablePets.DebugPrint("GRIMALKINP DEBUG: nocurse is now "..tostring(inst.nocurse))
end

local function ToggleBulletSpawns(inst)
	if inst.can_spawn_bullets then
		inst.can_spawn_bullets = false
	else
		inst.can_spawn_bullets = true
	end
	PlayablePets.DebugPrint("GRIMALKINP DEBUG: can_spawn_bullets is now "..tostring(inst.can_spawn_bullets))
end

local function RemoveMinion(minion)
	if minion.owner and minion.owner.minions and minion.owner.minions[minion] then
		minion.owner.minions[minion] = nil
		PlayablePets.DebugPrint("GRIMALKINP DEBUG: Minion is detaching from table, minions left: "..tostring(#minion.owner.minions))
	end	
end

local function SummonMinion(inst)
	local num_minions = 1
	local pos = inst:GetPosition()
	for i = 1, num_minions do
		local position = {x = pos.x + math.random(-20, 20), y = 0, z = pos.z + math.random(-20, 20)}
		if inst.minions and inst.components.leader:CountFollowers() < 2 then
			local fx = SpawnPrefab("sk_marg_lightning")
			fx.Transform:SetPosition(position.x, position.y, position.z)
			inst:DoTaskInTime(0.1, function(inst)
				local minion = SpawnPrefab("sk_spookat_black_t2")
				minion.can_summon = false
				inst.components.leader:AddFollower(minion)
				minion.Transform:SetPosition(position.x, position.y, position.z)
				minion.owner = inst
				minion.components.lootdropper:SetLoot({})
				inst.minions[minion] = minion
				minion:ListenForEvent("onremove", RemoveMinion)
				minion:ListenForEvent("death", RemoveMinion)
				minion.persists = false
			end)
		end	
	end	
	PlayablePets.DebugPrint("GRIMALKINP DEBUG: minioncount is now "..tostring(#inst.minions))
end

local states=
{
    
	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop")
			if math.random() < 0.15 and not inst.noactions and inst.sounds then
				inst.SoundEmitter:PlaySound(inst.sounds.moan)
			end
        end,
		
		events =
		{
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("idle")
			end)
		}
    },
	
	State{
        name = "special_atk1", --summon
        tags = {"busy", "canrotate"},
        onenter = function(inst)
			if inst.taunt and not inst.noactions then
				inst.Physics:Stop()
				inst.AnimState:PlayAnimation("action")
				SummonMinion(inst)
				
				inst.taunt = false
				if inst.taunt_task then
					inst.taunt_task:Cancel()
					inst.taunt_task = nil
				end
				inst.taunt_task = inst:DoTaskInTime(15, function(inst)
					inst.taunt = true
				end)
			else
				inst.sg:GoToState("idle")
			end
        end,
		
		onexit = function(inst)
			
		end,
		
		events =
		{
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("idle")
			end)
		}
    },
	
	State{
        name = "special_atk2", --toggle curse aura/effects
        tags = {"busy", "canrotate"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("action")
			ToggleCurseAura(inst)
        end,
		
		events =
		{
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("idle")
			end)
		}
    },
	State{
        name = "special_atk3", --toggle Bullet Spawns
        tags = {"busy", "canrotate"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("action")
			ToggleBulletSpawns(inst)
        end,
		
		events =
		{
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("idle")
			end)
		}
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.sg.mem.last_hit_time = GetTime()
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            --inst.SoundEmitter:PlaySound(inst.sounds.death)
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
            inst.AnimState:PlayAnimation("bite")
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))   
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        events =
        {
			
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst.noskeleton = true
                    PlayablePets.DoDeath(inst)
                end
            end),
        },      

    },    
	
	State{
        name = "attack", --dash
        tags = {"attack", "busy"},
		
		onenter = function(inst)
			if inst.noactions then
				inst.sg:GoToState("idle")
			else
				local buffaction = inst:GetBufferedAction()
				local target = buffaction ~= nil and buffaction.target or nil
				if target and target:IsValid() then
					inst.sg.statemem.attacktarget = target
					inst:FacePoint(target:GetPosition())
				end
				if inst.minions then
					inst:SuggestTarget(inst.sg.statemem.attacktarget)
				end
				if inst:HasTag("blackkat") then
					inst.SoundEmitter:PlaySound(inst.sounds.grunt)
				end
				inst.components.combat:StartAttack()
				inst.components.locomotor:Stop()
				inst.Physics:Stop()
	
				inst.AnimState:PlayAnimation("bite")
			end
		end,
	
		onexit = function(inst)
			SetInvisible(inst)
		end,
	
		timeline=
		{
			TimeEvent(2*FRAMES, function(inst) 
				local fx = SpawnPrefab("sk_bite_fxp")
				local posx, posy, posz = 0, 0, 0
				local angle = -inst.Transform:GetRotation() * DEGREES
				local offset = 1
				local targetpos = {x = posx + offset, y = 1, z = posz} 
				fx.AnimState:SetMultColour(1, 26/255, 1, 1)
				fx.entity:SetParent(inst.entity)
				fx.Transform:SetPosition(targetpos.x, targetpos.y, targetpos.z)		
			end),
			TimeEvent(8 * FRAMES, function(inst)
				DoBiteAoe(inst, inst.sg.statemem.attacktarget or nil)
				inst.SoundEmitter:PlaySound(inst.sounds.attack)
			end),
		},

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
	
    },
	------------------SLEEPING-----------------
	State {
        name = "special_sleep",
        tags = { "busy", "sleeping" }, 

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			
			if inst.noactions then
				SetVisible(inst)
				inst.AnimState:PlayAnimation("activate")
				inst.SoundEmitter:PlaySound(inst.sounds.spawn)
			else
				inst.AnimState:PlayAnimation("bite")
				if inst:HasTag("blackkat") then
					inst.SoundEmitter:PlaySound(inst.sounds.grunt)
				end
				inst.sg.statemem.disappearing = true
			end
        end,
		
		timeline = 
		{
			TimeEvent(2*FRAMES, function(inst) 
				if inst.sg.statemem.disappearing then
				local fx = SpawnPrefab("sk_bite_fxp")
				local posx, posy, posz = 0, 0, 0
				local angle = -inst.Transform:GetRotation() * DEGREES
				local offset = 1
				local targetpos = {x = posx + offset, y = 1.5, z = posz} 
				fx.AnimState:SetMultColour(1, 26/255, 1, 1)
				fx.entity:SetParent(inst.entity)
				fx.Transform:SetPosition(targetpos.x, targetpos.y, targetpos.z)	
				end
			end),
			TimeEvent(8 * FRAMES, function(inst)
				if inst.sg.statemem.disappearing then
				--DoBiteAoe(inst)
				inst.SoundEmitter:PlaySound(inst.sounds.attack)
				end
			end),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
		
		onexit = function(inst)
			if inst.sg.statemem.disappearing then
				SetInvisible(inst)
			end
		end,
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("idle_loop")			
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("idle_loop")
			if math.random() < 0.15 and not inst.noactions then
				inst.SoundEmitter:PlaySound(inst.sounds.moan)
			end
        end,
		
		timeline = {
		
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("idle_loop")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },		
	
	State
    {
        name = "eat",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("eating")	
			inst:PerformBufferedAction()
			inst.Physics:Stop()
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}




--CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"action", nil, nil, "idle_loop", "action") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{

		},
		
		corpse_taunt =
		{

		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "action"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
	
PP_CommonStates.AddJumpInStates(states, nil, "action")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "action", "action")
local simpleanim = "action"
local simpleidle = "idle_loop"
local simplemove = "idle_loop"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simpleanim,
	plank_hop = simpleanim,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = simpleanim,
	
	leap_pre = simpleanim,
	leap_loop = "idle_loop",
	leap_pst = simpleanim,
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	parry_loop = "idle_loop",
	
	castspelltime = 10,
})

    
return StateGraph("grimalkinp", states, events, "idle", actionhandlers)

