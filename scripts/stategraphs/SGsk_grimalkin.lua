require("stategraphs/commonstates")
require("stategraphs/ppstates")
local t_values = TUNING.SK.GRIMALKIN
local MAX_SPEED = t_values.MAX_SPEED
local ACCELERATION = MAX_SPEED/100 -- TODO how was this determined?

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "spookat", "undead", "grimalkin", "wall"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "shadow","spookat", "undead", "grimalkin", "wall"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "shadow", "spookat", "undead", "grimalkin", "wall"}
	end
end

local actionhandlers = {}

local function RemoveMinion(minion)
	if minion.owner and minion.owner.minions and minion.owner.minions[minion] then
		minion.owner.minions[0] = nil
	end
end

local function SpawnMinions(inst)
	local num_minions = 1
	local pos = inst:GetPosition()
	for i = 1, num_minions do
		local position = {x = pos.x + math.random(-5, 5), y = 0, z = pos.z + math.random(-5, 5)}
		if inst.minions and #inst.minions < 2 and not IsOceanTile(TheWorld.Map:GetTileAtPoint(position.x, position.y, position.z)) then
			local minion = SpawnPrefab("sk_spookat_black")
			inst.components.leader:AddFollower(minion)
			minion.Transform:SetPosition(position.x, position.y, position.z)
			minion.owner = inst
			minion.components.lootdropper:SetLoot({})
			table.insert(inst.minions, minion)
			minion:ListenForEvent("onremove", RemoveMinion)
			minion:ListenForEvent("death", RemoveMinion)
			minion.persists = false
		end		
	end	
end

local function ExplosionShake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .03, 1, inst, 20)
end

local events=
{
    EventHandler("attacked", function(inst) 
        if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("parrying") then 
            if not inst.sg:HasStateTag("attack") and not inst.isblocked and (inst.sg.mem.last_hit_time or 0) + inst.hit_recovery*2 < GetTime() and not inst.sg:HasStateTag("nointerrupt") then -- don't interrupt attack or exit shield
                inst.sg:GoToState("hit") -- can still attack
            end
			inst.isblocked = nil
        end 
    end),
    EventHandler("doattack", function(inst, data) 
        if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then 
            inst.sg:GoToState("attack", data.target) 
        end 
    end),
    EventHandler("death", function(inst, data)
		if inst:HasTag("epic") then
			inst.sg:GoToState("death")
		else
			inst.sg:GoToState("attack")
		end
    end),
}

local function SetShield(inst, shield)
	if shield then
		inst.AnimState:SetMultColour(255, 50, 50, 1)
	else
		inst.AnimState:SetMultColour(1, 1, 1, 1)
	end	
end

local function SetInvisible(inst)
	if inst:HasTag("epic") then
		inst.components.health:SetInvincible(true)
		inst:Hide()
	else
		inst:Remove()
	end
end

local function SetVisible(inst)
	inst.components.health:SetInvincible(false)
	inst:Show()
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"notarget", "INLIMBO", "shadow"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget", "shadow"}
	else	
		return {"player", "companion", "INLIMBO", "notarget"}
	end
end

local function DoBiteAoe(inst)
	local posx, posy, posz = inst.Transform:GetWorldPosition()
	local angle = -inst.Transform:GetRotation() * DEGREES
	local offset = t_values.BITE_OFFSET
	local targetpos = {x = posx + (offset * math.cos(angle)), y = 0, z = posz + (offset * math.sin(angle))} 
	local ents = TheSim:FindEntities(targetpos.x, 0, targetpos.z, t_values.AOE_RANGE, {"_combat"}, GetExcludeTags(inst))
	for _,ent in ipairs(ents) do
		if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health then
			inst:PushEvent("onareaattackother", { target = ent--[[, weapon = self.inst, stimuli = self.stimuli]] })
			ent.components.combat:GetAttacked(inst, 1)
			ent.components.health:DoDelta(-75, false, nil, nil, inst, true)
		end
	end
end

local states=
{
    
	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop")
			if math.random() < 0.15 and not inst.noactions and inst.sounds then
				inst.SoundEmitter:PlaySound(inst.sounds.moan)
			end
        end,
		
		onupdate = function(inst)
			inst.current_speed = math.min(inst.current_speed + ACCELERATION, MAX_SPEED)
			if inst.current_speed then
				inst.Physics:SetMotorVel(inst.current_speed, 0, 0)
			end
		end,
		
		events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.sg.mem.last_hit_time = GetTime()
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            --inst.SoundEmitter:PlaySound(inst.sounds.death)
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
            inst.AnimState:PlayAnimation("bite")
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))   
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        events =
        {
			
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.noskeleton = true
                    PlayablePets.DoDeath(inst)
                end
            end),
        },      

    },    
	
	State{
        name = "attack", --dash
        tags = {"attack", "busy"},
		
		onenter = function(inst)
			inst.brain:Stop()
			inst.Physics:SetMotorVel(inst.current_speed, 0, 0)
			if inst:HasTag("blackkat") then
				inst.SoundEmitter:PlaySound(inst.sounds.grunt)
			end
			--inst.components.combat:StartAttack()	
			inst.AnimState:PlayAnimation("bite")
		end,
		
		onupdate = function(inst)
			if inst.current_speed then
				inst.Physics:SetMotorVel(inst.current_speed/2, 0, 0)
			end
		end,
	
		onexit = function(inst)
			SetInvisible(inst)
		end,
	
		timeline=
		{
			TimeEvent(2*FRAMES, function(inst) 
				local fx = SpawnPrefab("sk_bite_fxp")
				local posx, posy, posz = 0, 0, 0
				local angle = -inst.Transform:GetRotation() * DEGREES
				local offset = 1
				local targetpos = {x = posx + offset, y = 1, z = posz} 
				fx.AnimState:SetMultColour(1, 26/255, 1, 1)
				fx.entity:SetParent(inst.entity)
				fx.Transform:SetPosition(targetpos.x, targetpos.y, targetpos.z)		
			end),
			TimeEvent(8 * FRAMES, function(inst)
				DoBiteAoe(inst)
				inst.SoundEmitter:PlaySound(inst.sounds.attack)
			end),
		},

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State
    {
        name = "eat",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("eating")	
			inst:PerformBufferedAction()
			inst.Physics:Stop()
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "spawn",
        tags = { "busy"},

        onenter = function(inst)
			inst.current_speed = 0
            inst.AnimState:PlayAnimation("activate")	
			inst.SoundEmitter:PlaySound(inst.sounds.spawn)
			inst.Physics:Stop()
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}
    
return StateGraph("sk_grimalkin", states, events, "spawn", actionhandlers)