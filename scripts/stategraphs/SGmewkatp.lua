require("stategraphs/commonstates")
require("stategraphs/ppstates")

local MAX_SPEED = 10 -- TODO adjust?
local MAX_DASH_TIME = 0.6 -- seconds?
local ACCELERATION = MAX_SPEED/12 -- TODO how was this determined?

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "shadow","kat", "undead"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	end
end

function TableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function SetSleeperAwakeState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:RemoveImmunity("sleeping")
    end
	if inst.components.shedder ~= nil then
	inst.components.shedder:StartShedding(360)
	end
	
    if inst.components.talker ~= nil then
        inst.components.talker:StopIgnoringAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Enable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(true)
        inst.components.playercontroller:Enable(true)
    end
    inst:OnWakeUp()
    inst.components.inventory:Show()
    inst:ShowActions(true)
	--inst.sg:GoToState("taunt")
end

local function SetSleeperSleepState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:AddImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:IgnoreAll("sleeping")
    end
	
	if inst.components.shedder ~= nil then
	inst.components.shedder:StopShedding()
	end
	
    if inst.components.firebug ~= nil then
        inst.components.firebug:Disable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(false)
        inst.components.playercontroller:Enable(false)
    end
    inst:OnSleepIn()
    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")
    inst:ShowActions(false)
end

local function CalcChompSpeed(inst, target)
	--local target = inst.components.combat.target
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (target.Physics ~= nil and inst.Physics:GetRadius() + target.Physics:GetRadius() or inst.Physics:GetRadius())
        if dist > 0 then
            return inst:HasTag("blackkat") and math.min(8, dist) / (28 * FRAMES) or math.min(8, dist) / (17 * FRAMES)
        end
    end
    return 0
end

local function RemoveMinion(minion)
	if minion.owner and minion.owner.minions and minion.owner.minions[minion] then
		minion.owner.minions[0] = nil
	end
end

local function SpawnMinions(inst)
	local num_minions = 2
	local pos = inst:GetPosition()
	for i = 1, num_minions do
		local position = {x = pos.x + math.random(-5, 5), y = 0, z = pos.z + math.random(-5, 5)}
		if inst.minions and #inst.minions < 4 and not IsOceanTile(TheWorld.Map:GetTileAtPoint(position.x, position.y, position.z)) then
			local minion = SpawnPrefab(math.random() < 0.1 and "sk_bombie" or "sk_zombie")
			inst.components.leader:AddFollower(minion)
			minion.Transform:SetPosition(position.x, position.y, position.z)
			minion.owner = inst
			minion.components.lootdropper:SetLoot({})
			table.insert(inst.minions, minion)
			minion:ListenForEvent("onremove", RemoveMinion)
			minion:ListenForEvent("death", RemoveMinion)
			minion.persists = false
		end		
	end	
end

local function ExplosionShake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .03, 1, inst, 20)
end

local events=
{
	PP_CommonHandlers.AddCommonHandlers(),
	--[[
    EventHandler("attacked", function(inst) 
        if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("parrying") then 
            if not inst.sg:HasStateTag("attack") and not inst.isblocked and (inst.sg.mem.last_hit_time or 0) + inst.hit_recovery*2 < GetTime() and not inst.sg:HasStateTag("nointerrupt") then -- don't interrupt attack or exit shield
                inst.sg:GoToState("hit") -- can still attack
            end
			inst.isblocked = nil
        end 
    end),]]
    EventHandler("doattack", function(inst, data) 
        if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then 
            inst.sg:GoToState("attack", data.target) 
        end 
    end),
    EventHandler("death", function(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end

        if (data ~= nil and data.cause == "file_load") or inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
			if inst:HasTag("_isinrez") then
				inst.sg:GoToState("fake_death")
			else
				inst.sg:GoToState("death")
			end
        end
    end),
	EventHandler("respawnfromcorpse", function(inst, reviver) 
		if inst.sg:HasStateTag("death") then 
			inst.sg:GoToState("corpse_rebirth") 
		end 
	end),	
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(),
    EventHandler("entershield", function(inst) inst.sg:GoToState("shield") end),
    EventHandler("exitshield", function(inst) inst.sg:GoToState("shield_end") end),
    
	
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end

local function GetAnimation(inst, str)
	return inst.isangry and str.."_angry" or str
end

local function SetAngry(inst, mood)
	inst.isangry = false
	
end

local states=
{
    
	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation(GetAnimation(inst, "idle_loop"), true)
        end,
		
		events =
		{
			EventHandler("animover", function(inst) 
				if inst.isangry then
					inst.sg:GoToState("idle")
				else
					inst.sg:GoToState(math.random() < 0.2 and "special_atk1" or "idle")
				end
			end)
		}
    },
	
	State{
        name = "special_atk1",
        tags = {},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("idle_taunt")
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			
			SetAngry(inst, false)
        end,

        timeline=
        {
			TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.taunt) end)
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.sg.mem.last_hit_time = GetTime()
			SetAngry(inst, true)
			--inst.SoundEmitter:PlaySound(inst.sounds.hit)
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.death)
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			inst.DynamicShadow:Enable(false)
            inst.AnimState:PlayAnimation("death")
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))   
			inst.components.inventory:DropEverything(true)
			
			inst.sg:SetTimeout(2)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.noskeleton = true
                    PlayablePets.DoDeath(inst)
                end
            end),
        },      
		
		ontimeout = function(inst)
			inst.SoundEmitter:KillSound("ghost_loop")
            if MOBGHOST == "Enable" then
                inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = false })
			else
				TheWorld:PushEvent("ms_playerdespawnanddelete", inst)
			end
		end,

    },    
	
	State{
        name = "special_atk2", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		inst.components.locomotor:Stop()
		inst.Physics:Stop()
		if inst:HasTag("blackkat") then
			inst.SoundEmitter:PlaySound(inst.sounds.grunt)
			inst.SoundEmitter:PlaySound(inst.sounds.spit)
			inst.AnimState:PlayAnimation("attack_spit")
			inst.taunt2 = false
			if inst.taunt2_task then
				inst.taunt2_task:Cancel()
				inst.taunt2_task = nil
			end
			inst.taunt2_task = inst:DoTaskInTime(20, function(inst) inst.taunt2 = true end)
		else
			if inst.isangry then
				inst.AnimState:PlayAnimation("angry_to_idle")
				SetAngry(inst, false)
			else
				inst.AnimState:PlayAnimation("idle_to_angry")
				SetAngry(inst, true)
			end
		end
	end,
	
	onexit = function(inst)
		
    end,
	
	onupdate = function(inst)
        
    end,
	
    timeline=
    {
		TimeEvent(18*FRAMES, function(inst) 
				
		end),
        TimeEvent(23*FRAMES, function(inst) 
			if inst:HasTag("blackkat") then
				SpawnMinions(inst)
				inst.Physics:Stop()
			end
		end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	------------------SLEEPING-----------------
	State {
        name = "sleep",
        tags = { "busy", "sleeping" }, 

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
            inst.AnimState:PlayAnimation(GetAnimation(inst, "sleep_pre"))
        end,
		
		timeline = 
		{
			
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
		
		onexit = function(inst)

		end,
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation(GetAnimation(inst, "sleep_loop"))
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline =
		{
			
		},
		
		onexit = function(inst)

		end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			inst.AnimState:PlayAnimation(GetAnimation(inst, "sleep_pst"))
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,
		
		timeline = 
		{

		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
		
		onexit = function(inst)

		end,
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst.isangry then
				inst.components.locomotor:RunForward()
			else
				inst.components.locomotor:WalkForward()
			end
            inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_pre"))			
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst.isangry then
				inst.components.locomotor:RunForward()
			else
				inst.components.locomotor:WalkForward()
			end
			if inst:HasTag("blackkat") and math.random() < 0.1 then
				inst.SoundEmitter:PlaySound(inst.sounds.taunt)
			end
			inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_loop"))
        end,
		
		timeline = {
		
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_pst"))            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },		
	
	State
    {
        name = "run_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst.isangry then
				inst.components.locomotor:RunForward()
			else
				inst.components.locomotor:WalkForward()
			end
            inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_pre"))			
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "canrotate", "running" },

        onenter = function(inst)
			if inst.isangry then
				inst.components.locomotor:RunForward()
			else
				inst.components.locomotor:WalkForward()
			end
			if inst:HasTag("blackkat") and math.random() < 0.1 then
				inst.SoundEmitter:PlaySound(inst.sounds.taunt)
			end
			inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_loop"))
        end,
		
		timeline = {
		
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_pst"))            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}




CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{

		},
		
		corpse_taunt =
		{

		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "idle_taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
	
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death_old")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pre",
	leap_loop = "walk_loop",
	leap_pst = "walk_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	parry_loop = "idle_loop",
	
	castspelltime = 10,
})

    
return StateGraph("mewkatp", states, events, "idle", actionhandlers)

