
return	{
	
	ZOMBIE = {
	
		HEALTH = 300,
	
		DAMAGE = 80,
		ATTACKPERIOD = 2,
		
		ATTACK_RANGE = 2,
		HIT_RANGE = 3,
	
		RUNSPEED = 9,
		WALKSPEED = 2.5,
		
		SPECIAL_CD = 10,
	},
	
	SPOOKAT_BLACK = {
	
		HEALTH = 300,
	
		DAMAGE = 80,
		ATTACKPERIOD = 2,
		
		ATTACK_RANGE = 2,
		HIT_RANGE = 3,
	
		RUNSPEED = 9,
		WALKSPEED = 2.5,
		
		SPECIAL_CD = 10,
	},
	
	BOMBIE = {
	
		HEALTH = 125,
	
		DAMAGE = 400,
		ATTACKPERIOD = 2,
		
		ATTACK_RANGE = 2,
		HIT_RANGE = 4,
	
		RUNSPEED = 9,
		WALKSPEED = 2.5,
		
		SPECIAL_CD = 10,
	},
	
	--======================================
	--				Weapons
	--======================================
	--[[
	POOPWEAPON = {
	
		DAMAGE = 30,
		ALT_DAMAGE = 30,
		
	},]]
}