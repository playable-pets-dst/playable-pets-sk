
------------------------------------------------------------------
-- Variables
------------------------------------------------------------------
local require = GLOBAL.require
local Ingredient = GLOBAL.Ingredient

local PlayablePets = GLOBAL.PlayablePets
local SETTING = GLOBAL.PP_SETTINGS
local MOBTYPE = GLOBAL.PP_MOBTYPES

local STRINGS = require("ppsk_strings")

local TUNING = require("ppsk_tuning")


GLOBAL.PPSK_FORGE = require("ppsk_forge")

GLOBAL.SK_FAMILIES = {
	GREMLIN = 
	{
		family = "gremlin", --variable
		tag = "gremlin", --might not give to players?
		piercing_stat = 1,
		elemental_stat = 0.5,
		normal_stat = 1,
		shadow_stat = 2
	},
	SLIME = 
	{
		family = "slime", --variable
		tag = "slime", --might not give to players?
		piercing_stat = 0.5,
		elemental_stat = 1,
		normal_stat = 1,
		shadow_stat = 2,
		
		stun_resist = 0.5,
	},
	BEAST = 
	{
		family = "beast", --variable
		tag = "beast", --might not give to players?
		piercing_stat = 2,
		elemental_stat = 0.5,
		normal_stat = 1,
		shadow_stat = 1
	},
	UNDEAD = 
	{
		family = "undead", --variable
		tag = "undead", --might not give to players?
		piercing_stat = 1,
		elemental_stat = 2,
		normal_stat = 1,
		shadow_stat = 0.5,
		
		curse_resist = 1,
	},
	FIEND = 
	{
		family = "fiend", --variable
		tag = "fiend", --might not give to players?
		piercing_stat = 2,
		elemental_stat = 1,
		normal_stat = 1,
		shadow_stat = 0.5
	},
	CONSTRUCT = 
	{
		family = "construct", --variable
		tag = "construct", --might not give to players?
		piercing_stat = 0.5,
		elemental_stat = 2,
		normal_stat = 1,
		shadow_stat = 1,
		
		sleep_resist = 0,
	},
	UNKNOWN = --default or used for custom classes, like Vanaduke. 
	{
		family = "", --these top two won't be used
		tag = "", 
		piercing_stat = 1,
		elemental_stat = 1,
		normal_stat = 1,
		shadow_stat = 1
	},
	
}

------------------------------------------------------------------
-- Configuration Data
------------------------------------------------------------------

PlayablePets.Init(env.modname)

GLOBAL.SK_DIFFICULTY = GetModConfigData("sk_difficulty")
GLOBAL.SK_CURSE_ENABLE = GetModConfigData("sk_curse_enable")

GLOBAL.SK_HEALTH = require("sk_health")
--GLOBAL.GVG_MODE = PlayablePets.GetModConfigData("GvGMode")
------------------------------------------------------------------
-- Prefabs
------------------------------------------------------------------

PrefabFiles = {
	"sk_zombie",
	"sk_bombie",
	"sk_spookat",
	"sk_grimalkin",
	------Misc-------
	"sk_grim_totem",
	"sk_lantern",
	"sk_gravestone",
	-------FX--------
	"sk_debuffs",
	"sk_bullet",
	"sk_zombie_breath_fx",
	"sk_fx",
	"sk_auras",
	"sk_marg_util",
	
}

-- Centralizes a number of related functions about playable mobs to one convenient table
-- Name is the prefab name of the mob
-- Fancyname is the name of the mob, as seen in-game
-- Gender is fairly self-explanatory
-- Skins is a list of any custom mob skins that may be available, using the Modded Skins API by Fidoop
-- Skins are not required for mobs to function, but they will display a custom portrait when the mob is examined by a player
GLOBAL.PPSK_MobCharacters = {
	zombie_dustp        = { fancyname = "Dust Zombie",           gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true},
	zombie_firep        = { fancyname = "Slag Walker",           gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true},
	zombie_freezep        = { fancyname = "Frozen Shambler",           gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true},
	zombie_poisonp        = { fancyname = "Droul",           gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true},
	zombie_shockp        = { fancyname = "Frankenzom",           gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true},
	zombie_cursep        = { fancyname = "Carnavon",           gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true},
	bombie_dustp        = { fancyname = "Bombie",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	bombie_firep        = { fancyname = "Burning Bombie",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	bombie_freezep        = { fancyname = "Freezing Bombie",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	bombie_poisonp        = { fancyname = "Choking Bombie",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	bombie_shockp        = { fancyname = "Surging Bombie",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	--
	spookat_nrmp        = { fancyname = "Spookat",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	spookat_shockp        = { fancyname = "Statikat",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	spookat_poisonp        = { fancyname = "Hurkat",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	spookat_firep        = { fancyname = "Pepperkat",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	spookat_freezep        = { fancyname = "Pepperkat",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	spookat_blackp        = { fancyname = "Black Kat",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	mewkatp        = { fancyname = "Mew Kat",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	grimalkinp        = { fancyname = "Grimalkin",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	grimalkin_cursep        = { fancyname = "Margrel",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
}



-- Necessary to ensure a specific order when adding mobs to the character select screen. This table is iterated and used to index the one above
PPSK_Character_Order = {"zombie_dustp", "zombie_firep", "zombie_freezep", "zombie_poisonp", "zombie_shockp", "zombie_cursep", 
"bombie_dustp", "bombie_firep", "bombie_freezep", "bombie_poisonp", "bombie_shockp",
"spookat_nrmp", "spookat_shockp", "spookat_poisonp", "spookat_firep", "spookat_freezep", "spookat_blackp", "mewkatp", "grimalkinp", "grimalkin_cursep", 
}

------------------------------------------------------------------
-- Assets
------------------------------------------------------------------

Assets = {
	Asset("ANIM", "anim/ghost_monster_build.zip"), -- Might make it not needed on every prefab.
	
	Asset("SOUNDPACKAGE", "sound/sk.fev"), --make sure to update this every time SW updates.
	Asset( "SOUND", "sound/sk.fsb"), --Needed until SW gets ported to DST.
	--------------------------HOMES--------------------------------
}

------------------------------------------------------------------
-- Custom Recipes
------------------------------------------------------------------

------------------------------------------------------------------
-- Component Overrides
------------------------------------------------------------------

------------------------------------------------------------------
-- PostInits
------------------------------------------------------------------	

-------------------------------------------------------
--Wardrobe stuff--

--Skin Puppet Stuff
local MobPuppets = require("ppsk_puppets")
local MobSkins = require("ppsk_skins")
PlayablePets.RegisterPuppetsAndSkins(PPSK_Character_Order, MobPuppets, MobSkins)
------------------------------------------------------------------
-- Commands
------------------------------------------------------------------

------------------------------------------------------------------
-- Asset Population
------------------------------------------------------------------

local assetPaths = { "bigportraits/", "images/map_icons/", "images/avatars/avatar_", "images/avatars/avatar_ghost_" }
local assetTypes = { {"IMAGE", "tex"}, {"ATLAS", "xml"} }

-- Iterate through the player mob table and do the following:
-- 1. Populate the PrefabFiles table with the mob prefab names and their skin prefabs (if applicable)
-- 2. Add an atlas and image for the mob's following assets:
-- 2.1 Character select screen portraits
-- 2.2 Character map icons
-- 2.3 ??? FIXME
-- 2.4 ??? FIXME
--for prefab, mob in pairs(GLOBAL.PPSK_MobCharacters) do
for _, prefab in ipairs(PPSK_Character_Order) do
	local mob = GLOBAL.PPSK_MobCharacters[prefab]
	if PlayablePets.MobEnabled(mob, env.modname) then
		table.insert(PrefabFiles, prefab)
	end
	
	-- Add custom skin prefabs, if available
	-- Example: "dragonplayer_formal"
	for _, skin in ipairs(mob.skins) do
			table.insert(PrefabFiles, prefab.."_"..skin)
	end
	
	for _, path in ipairs(assetPaths) do
		for _, assetType in ipairs(assetTypes) do
			--print("Adding asset: "..assetType[1], path..prefab.."."..assetType[2])
			table.insert( Assets, Asset( assetType[1], path..prefab.."."..assetType[2] ) )
		end
	end
end

------------------------------------------------------------------
-- Mob Character Instantiation
------------------------------------------------------------------

-- Adds a mod character based on an individual mob
-- prefab is the prefab name (e.g. clockwork1player)
-- mob.fancyname is the mob's ingame name (e.g. Knight)
-- mob.gender is fairly self-explanatory
--for prefab, mob in pairs(GLOBAL.PPSK_MobCharacters) do
for _, prefab in ipairs(PPSK_Character_Order) do
	local mob = GLOBAL.PPSK_MobCharacters[prefab]
	PlayablePets.SetGlobalData(prefab)
	if PlayablePets.MobEnabled(mob, env.modname) then
		AddMinimapAtlas("images/map_icons/"..prefab..".xml")
		AddModCharacter(prefab, mob.gender)
	end
end